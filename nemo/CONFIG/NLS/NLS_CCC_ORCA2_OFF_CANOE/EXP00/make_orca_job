#!/bin/sh
#
#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
# Create a job string that will run NEMO
#
# Neil Swart Jul 2014 : update to new xnemo.
#
# Larry Solheim ...Mar 2012
# $Id: make_nemo_job 673 2012-06-05 20:39:40Z acrnrls $
#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#

#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
# Commonly changed parameters
#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#

# runid is assigned to the namelist variable cn_exp
runid=abc

# Output file names will begin with ${uxxx}_${runid}_
uxxx=mc

# Set the start and stop dates of the current run segment
# The values for start and stop must be of the form
#      Year
#   or Year:Month
# where Year and Month are integers. Month will default
# to 1 for start and 12 for stop if it is not present.
start=001
stop=010

# Set start year for rtd
year_rtdiag_start=001

# The name of a tar file containing the NEMO restart files
# This must be set for the first job of a run unless nemo_from_rest = on
# Restart files created by the submission job will have names of the form
#     ${uxxx}_${runid}_YYYYMMDD_restart.tar
nemo_restart=''

# nemo_from_rest is used to flag starting nemo from rest rather than a restart
# If this is set "on" then the user must also supply namelist files via the
# variables nemo_namelist and nemo_namelist_ice.
# Either define nemo_restart or set nemo_from_rest = on but not both.
nemo_from_rest=on

# nemo_exec identifies the nemo executable
# This can be a file that is "save"d on DATAPATH or a full pathname to a user supplied binary.
# If it is a full path name then this path must be available from the execution machine.
# This file is saved in the reastart archive.
# If nemo_exec is not defined then the executable found in the restart will be used.
nemo_exec=${runid}_ccc_orca2_off_canoe_exe

# nemo_freq is the number of years (or months) nemo will run for each job in the string
# History files and restarts will be deleted after each job in the string.
# nemo_freq will be assigned a value of the form Ny or Nm where N is a positive integer.
nemo_freq=1y

# If do_nemo_run=on then access all the required files and do the run, else the model
# itself is not run.  Using do_nemo_run=off can be used, for example, to process 
# RTD for a run that has already been done and has files of RUNPATH, or to delete
# history files after a run has been done.
do_nemo_run=on

# nemo_forcing identifies the type of forcing to use
# Currenlty the only valid values are "bulk" or "flux"
nemo_forcing=bulk

# nemo_carbon = off means use physical ocean only
# nemo_carbon = on  means include PISCES
nemo_carbon=on

# pisces_offline = off means use online model
# pisces_offline = on  means use offline model with prescribed velocities
pisces_offline=on

# Concatenate history files into one file per year
# (Required for RTD). The sub-annual files will be deleted.
nemo_ann_cat=on

# RTD switch. If nemo_carbon=on, both physical and carbon RTD will be done,
# otherwise only physical. User can specify rtd exectuables below. 
nemo_rtd=on

# Dump history files saved on DATAPATH to cfs
# The user can redefine nemo_dump_hist_freq_list and nemo_dump_hist_suffix_list
# to only dump a subset of the history files
nemo_dump_hist=on

# Dump restart archives saved on DATAPATH to cfs
# The user can redefine nemo_dump_rs_month_list. Restarts will
# only be dumped for those months listed in nemo_dump_rs_month_list.
nemo_dump_rs=on

# Delete history files saved on DATAPATH.
# The user can redefine nemo_del_hist_freq_list and nemo_del_hist_suffix_list
# to only delete a subset of the history files
nemo_del_hist=on

# If nemo_del_rs=on, then delete the previous restart tar archive from DATAPATH.
nemo_del_rs=on

# cfs related variables
shortermdir=off
masterdir=on

# Return stdout to $HOME/.queue when noprint = off
noprint=on

# nn_ice = 0 or 1: with_ice=off; nn_ice=2,3,4: with_ice=on (in xnemo).
# nn_ice = 0 no ice boundary condition
#          1 use observed ice-cover;
#          2 LIM2
#          3 LIM3
#          4 CICE
nn_ice=2

# time limit
# try to make sure we go to the single Q if not running the model.
if [ "x$do_nemo_run" = "xoff" ]; then
  time=1799
else
  time=3600
fi

# Any command line args of the form var=val will be added to the current environment.
# These values will override that of any of the variables that are set above.
# All command line args that are not of the form var=val are ignored.
for arg in "$@"; do
  case $arg in
    *=*) var=`echo $arg|awk -F\= '{printf "%s",$1}' -`
         val=`echo "$arg"|awk '{i=index($0,"=")+1;printf "%s",substr($0,i)}' -`
         [ -n "$var" ] && eval ${var}=\"\$val\" # preserve quoted assignments
         ;;
  esac
done

# Ensure that we have compatible nemo_from_rest and nemo_restart definitions
if [ x"$nemo_from_rest" = xon -a -n "$nemo_restart" ]; then
  echo "You cannot set nemo_from_rest=on and also define nemo_restart."
  exit 1
fi

#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
# End of commonly changed parameters
#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#

#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
# Other parameters that may also be changed
#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#

this_usr=`whoami`
case $this_usr in
    acrnocn) runpath=/data/cava_ccrnj01/data ;;
    acrncbn) runpath=/data/cava_ccrnj03/data ;;
    acrngcm) runpath=/data/cava_ccrnj13/data ;;
    acrnesm) runpath=/data/cava_ccrnj10/data ;;
    acrncc2) runpath=/data/cava_ccrnj05/data ;;
    acrnrwl) runpath=/data/cava_ccrnj13/data ;;
    acrnice) runpath=/data/cava_ccrnj13/data ;;
    acrnxxx) runpath=/my/alt/run/path ;;  # This could be you
          *) runpath='' ;;
esac
if [ -z "$runpath" ]; then
    echo "Using default front end RUNPATH"
    opt_RUNPATH=''
    opt_CCRNTMP=''
else
    # mdump_RUNPATH and mdump_CCRNTMP are the RUNPATH and CCRNTMP
    # used on the front end for the dumping/diagnostics/pooling/etc.
    opt_RUNPATH=mdump_RUNPATH=$runpath
    opt_CCRNTMP=mdump_CCRNTMP=$runpath/utmp
fi

# start_year, start_mon, stop_year and stop_mon are calculated here from start and stop
# Do not modify these definitions
start_year=`echo $start|awk -F':' '{printf "%04d",$1}' -`
start_mon=`echo $start|awk -F':' '{if ($2~/^[0-9][0-9]*$/) printf "%02d",$2; else print "01"}' -`
stop_year=`echo $stop|awk -F':' '{printf "%04d",$1}' -`
stop_mon=`echo $stop|awk -F':' '{if ($2~/^[0-9][0-9]*$/) printf "%02d",$2; else print "12"}' -`

# All variable definitions in the following here document will be passed to the
# cccjob command below and used to modify the job string for the current run.
job_defs=job_defs$$
cat <<end_job_defs >$job_defs
# Output file names will begin with ${uxxx}_${runid}_
  uxxx=$uxxx
  runid=$runid

# time step for the dynamics (and tracer if nn_acc=0)
  rn_rdt=21600.

# Define the nemo domain (implies node geometry for MPI)
  nemo_jpni=8
  nemo_jpnj=4
  nemo_jpnij=32

# Number of physical nodes used by this job
  phys_nodex=1

# Machine where dump jobs are running
  dump_sublist_besc=${dump_sublist_besc}

# nemo_from_rest is used to flag starting nemo from rest rather than a restart
# If this is set "on" then the user must also supply namelist files via the
# variables nemo_namelist and nemo_namelist_ice
  nemo_from_rest=$nemo_from_rest

# The name of a tar file containing the NEMO restart files
# This must be set for the first job of a run unless nemo_from_rest = on
  nemo_restart=$nemo_restart

# nemo_exec identifies the nemo executable
# This can be a file that is "save"d on DATAPATH or a full pathname to a user supplied binary.
# If it is a full path name then this path must be available from the execution machine.
# This file is saved in the restart archive.
# If nemo_exec is not defined then the executable found in the restart will be used.
  nemo_exec=$nemo_exec

# Identify files containing namelist input for NEMO (namelist) and LIM (namelist_ice)
# These namelist files are only used when starting from rest or when nemo_force_namelist = on
# These can be a files that are "save"d on DATAPATH or full pathnames to any location.
# If they are full path names then these paths must be available from the execution machine.
  nemo_namelist=${runid}_ccc_orca2_off_canoe_namelist
  nemo_namelist_ice=${runid}_ccc_orca2_off_canoe_namelist_ice
  nemo_namelist_pisces=${runid}_ccc_orca2_off_canoe_namelist_pisces
  nemo_namelist_top=${runid}_ccc_orca2_off_canoe_namelist_top
# nn_ice = 0 no ice boundary condition
#          1 use observed ice-cover;
#          2 LIM2
#          3 LIM3
#          4 CICE
  nn_ice=${nn_ice}

# debug
debug=off

# time limit
  time=${time}

# Normally user suppled namelist files are only required when starting the model
# from rest. Otherwise the namelist files are stored in the restart archive and
# read from the archive at each restart. Setting nemo_force_namelist = on will
# over ride this behaviour and allow the namelists defined by the variables
# nemo_namelist and nemo_namelist_ice to be used at the start of each new string.
  nemo_force_namelist=off

# Define execution dir (CCRNTMP) and data dir (RUNPATH) used on the front end
  $opt_RUNPATH
  $opt_CCRNTMP

  shortermdir=$shortermdir
  masterdir=$masterdir
  with_lsarc=off
  nolist=on
  noprint=$noprint

# run_start_year is the first year of the run, which is normally start_year
# but can be reset by the user if required
  run_start_year=$start_year
  run_start_month=$start_mon

# Set start year for rtd
  year_rtdiag_start=${year_rtdiag_start}

# nemo_nn_it000 is the initial time step of the current run
# This is normally read from the previous restart but may be set here.
# If defined, nemo_nn_it000 must always be set to an integer greater than zero.
  nemo_nn_it000=''

# Access or copy files required to run NEMO
  orca_grid_info="nemo_3.4_orca2_mesh_mask.nc"
  nemo_iodef=${runid}_ccc_orca2_off_canoe_iodef.xml
  nemo_xmlio_server_def=${runid}_ccc_orca2_off_canoe_xmlio_server.def
  nemo_ahmcoef=nemo_3.3_orca2_ahmcoef
  nemo_coordinates=nemo_3.3_orca2_coordinates.nc
  nemo_bathy_meter=nemo_3.3_orca2_bathy_meter.nc
  nemo_mask_itf=nemo_3.3_orca2_mask_itf.nc
  nemo_M2rowdrg=nemo_3.3_orca2_m2rowdrg.nc
  nemo_K1rowdrg=nemo_3.3_orca2_k1rowdrg.nc
  nemo_geothermal_heating=nemo_3.3_orca2_geothermal_heating.nc
  nemo_runoff_core_monthly=nemo_3.3_orca2_runoff_core_monthly.nc
  nemo_sss_data=nemo_3.3_orca2_sss_data.nc
  nemo_chlorophyll=nemo_3.3_orca2_chlorophyll.nc
  nemo_subbasins=nemo_3.4_orca2_lim_subbasins.nc


# Access or copy files required to run PISCES
  nemo_bathy_orca=nemo_3.4_orca2_bathy.orca.nc
  nemo_data_alkalini_nomask=nemo_3.4_orca2_data_alkalini_nomask.nc
  nemo_data_dic_nomask=nemo_3.4_orca2_data_dic_nomask.nc
  nemo_data_doc_nomask=nemo_3.4_orca2_data_doc_nomask.nc
  nemo_data_fer_nomask=nemo_3.4_orca2_data_fer_nomask.nc
  nemo_data_no3_nomask=nemo_3.4_orca2_data_no3_nomask.nc 
  nemo_data_ori_nomask=uror_data_oriche_nomask.nc
  nemo_data_o2_nomask=nemo_3.4_orca2_data_o2_nomask.nc
  nemo_data_po4_nomask=nemo_3.4_orca2_data_po4_nomask.nc
  nemo_data_si_nomask=nemo_3.4_orca2_data_si_nomask.nc
  nemo_dust_orca=nemo_3.4_orca2_dust.orca.nc #cvu_1986_2005_fe_clim_nemo.nc
  nemo_ndeposition_orca=nemo_3.4_orca2_ndeposition.orca.nc
  nemo_presatm=nemo_3.4_orca2_presatm.nc
  nemo_river_orca=nemo_3.4_orca2_river.orca.nc


  nemo_data_1m_potential_temperature_nomask=nemo_3.3_orca2_data_1m_potential_temperature_nomask.nc
  nemo_data_1m_salinity_nomask=nemo_3.3_orca2_data_1m_salinity_nomask.nc

  nemo_t=nemo_3.3_orca2_t_10.15june2009_orca2.nc
  nemo_u=nemo_3.3_orca2_u_10.15june2009_orca2.nc
  nemo_v=nemo_3.3_orca2_v_10.15june2009_orca2.nc
  nemo_q=nemo_3.3_orca2_q_10.15june2009_orca2.nc
  nemo_qsw=nemo_3.3_orca2_ncar_rad.15june2009_orca2.nc
  nemo_qlw=nemo_3.3_orca2_ncar_rad.15june2009_orca2.nc
  nemo_precip=nemo_3.3_orca2_ncar_precip.15june2009_orca2.nc
  nemo_snow=nemo_3.3_orca2_ncar_precip.15june2009_orca2.nc
# weights_grid02_* are weights for ECMWF data
  nemo_weights_bicubic1=nemo_3.4_weights_grid02_bicubic_orca1.nc
  nemo_weights_bilinear1=nemo_3.4_weights_grid02_bilinear_orca1.nc
# weights_grid03_* are weights for CORE data
  nemo_weights_bicubic2=nemo_3.4_weights_grid03_bicubic_orca1.nc
  nemo_weights_bilinear2=nemo_3.4_weights_grid03_bilinear_orca1.nc
# weights_canam4grid_* are weights for CanAM4
# nemo_weights_bicubic2=nemo_3.4_weights_canam4grid_bicubic_orca1.nc
# nemo_weights_bilinear2=nemo_3.4_weights_canam4grid_bilinear_orca1.nc

# These are for flux forcing. If don't use flux forcing, provide any
# forcing data files existing on the disk. Only used in nemo_forcing=flux
  nemo_utau=nemo_3.4_orca1_t_10.15june2009.nc
  nemo_vtau=nemo_3.4_orca1_t_10.15june2009.nc
  nemo_qtot=nemo_3.4_orca1_t_10.15june2009.nc
  nemo_qsr=nemo_3.4_orca1_t_10.15june2009.nc
  nemo_emp=nemo_3.4_orca1_t_10.15june2009.nc
  nemo_weights_bicubic3=nemo_3.4_weights_grid03_bicubic_orca1.nc
  nemo_weights_bilinear3=nemo_3.4_weights_grid03_bilinear_orca1.nc

# Dynamics files for running offline
  dyna_grid_t=mc_nhr_1d_40010101_40011231_grid_t.nc
  dyna_grid_u=mc_nhr_1d_40010101_40011231_grid_u.nc
  dyna_grid_v=mc_nhr_1d_40010101_40011231_grid_v.nc
  dyna_grid_w=mc_nhr_1d_40010101_40011231_grid_w.nc
  offline_mesh_mask=uncs_mesh_mask.nc

# The boolean variable nemo_write_stdout can be used to control echoing of the
# file ocean.output (created by nemo as stdout) at runtime. When nemo_write_stdout
# is "on" this info will appear in the submission job output that is returned
# to a user's ~/.queue directory.
  nemo_write_stdout=on

# Nemo outputs data saved at different frequencies to different files.
# It will also distinguish output data by creating files with specific suffixes.
# The following variables will determine what subset of this data gets saved to
# DATAPATH after each run.
# 
# Saved nemo history files will have file names similar to
#     ${uxxx}_${runid}_${frq}_${start_date}_${stop_date}_${sfx}
# frq is the save frequency, one of "1y", "1m", "1d", "5d" (y=year, m=month, d=day)
# start_date and stop_date are determined at run time
# sfx is one of the suffixes shown below
# 
# By adding or removing suffixes from these lists or modifying the boolean variables
# nemo_save_*_data that are assigned below, the user can control nemo output.
# Note, case is significant for the suffixes in these lists.
# Any matching files that do not exist at run time will be silently ignored.
  nemo_1y_suffix_list="grid_T ptrc_T diad_T"
  nemo_1m_suffix_list="icemod grid_T grid_U grid_V grid_W ptrc_T diad_T"
  nemo_1d_suffix_list="grid_T grid_T_0n180w diaptr"
  nemo_5d_suffix_list="icemod grid_T grid_U grid_V grid_W"

# The following boolean variables will determine if any nemo output files with a
# particular save frequencies are saved to DATAPATH.
# If any of these are "off" then that set of files will not be saved regardless of
# the value of the corresponding nemo_*_suffix_list variable that is set above.
  nemo_save_1y_data=off
  nemo_save_1m_data=on
  nemo_save_1d_data=off
  nemo_save_5d_data=off

# If do_nemo_run=on then access all the required files and do the run, else the model
# itself is not run.  Using do_nemo_run=off can be used, for example, to process 
# RTD for a run that has already been done and has files of RUNPATH, or to delete
# history files after a run has been done.
  do_nemo_run=${do_nemo_run}

# The following determines if the bulk or flux formulation is being used:
  nemo_forcing=${nemo_forcing}

# The following determines if carbon is on or not:
  nemo_carbon=${nemo_carbon}

# The following determines if the offline model is used:
  pisces_offline=${pisces_offline}

# The following determines if history files are concatenated into yearly files.
# The user can redefine nemo_ann_cat_freq_list and nemo_ann_cat_suffix_list
# to only concatenate a subset of the history files
  nemo_ann_cat=${nemo_ann_cat}
  nemo_ann_cat_freq_list='1m'
  nemo_ann_cat_suffix_list='icemod grid_t grid_u grid_v grid_w ptrc_t diad_t'

# Determines if RTD is done, and specifies the RTD executables
  nemo_rtd=${nemo_rtd}
  physical_rtd_exe="nemo_physical_rtd_v03c_exe"
  carbon_rtd_exe="nemo_carbon-canoe_rtd_v03c_exe"
  ice_rtd_exe="nemo_ice_rtd_v03c_exe"

# Dump history files saved on DATAPATH to cfs
# The user can redefine nemo_dump_hist_freq_list and nemo_dump_hist_suffix_list
# to only dump a subset of the history files
  nemo_dump_hist=${nemo_dump_hist}
#  nemo_dump_hist_freq_list='1y 1m 1d 5d'
  nemo_dump_hist_freq_list='1m'
  nemo_dump_hist_suffix_list='icemod grid_t grid_u grid_v grid_w ptrc_t diad_t'

# Dump restart archives saved on DATAPATH to cfs
# The user can redefine nemo_dump_rs_month_list. Restarts will
# only be dumped for those months listed in nemo_dump_rs_month_list.
  nemo_dump_rs=${nemo_dump_rs}
  nemo_dump_rs_month_list='01 02 03 04 05 06 07 08 09 10 11 12'

# Delete history files saved on DATAPATH.
# The user can redefine nemo_del_hist_freq_list and nemo_del_hist_suffix_list
# to only delete a subset of the history files
  nemo_del_hist=${nemo_del_hist}
#  nemo_del_hist_freq_list='1y 1m 1d 5d'
  nemo_del_hist_freq_list='1m'
  nemo_del_hist_suffix_list='icemod grid_t grid_u grid_v grid_w ptrc_t diad_t'

# delete the previous restart.
  nemo_del_rs=$nemo_del_rs

end_job_defs

#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
# Create the job string from the template
#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#

outpfx=${runid}_${start_year}m${start_mon}_${stop_year}m$stop_mon

# Ensure save frequecy and nemo frequency have units "m"
nfq=`echo $nemo_freq | awk '
       /^ *[0-9]*y *$/ {sub(/y *$/,""); if ($0~/^ *$/){print "1y"}else{printf "%dm",$0*12}; exit}
       /^ *[0-9]*m *$/ {sub(/m *$/,""); if ($0~/^ *$/){print "1m"}else{printf "%dm",$0}; exit}' -`
[ -z "$nfq" ] &&
    echo "Invalid nemo_freq=${nemo_freq}. Should be an integer followed by y or m." && exit 1

JOBDESC="xnemo:$nfq"

if [ -z "$CCCJOB_ROOT" ]; then
    CCCJOB_ENV=''
else
    eval CCCJOB_ENV=\'env CCCJOB_ROOT\=$CCCJOB_ROOT\'
fi
$CCCJOB_ENV cccjob --jdef-repo=$CCRNSRC/ocean_dir/nemo-CCCma.git --jdef-rev=master --out=${outpfx}_job --job="$JOBDESC" --start="$start" --stop="$stop" $job_defs


rm -f $job_defs $dump_sublist_defs $del_list_defs

