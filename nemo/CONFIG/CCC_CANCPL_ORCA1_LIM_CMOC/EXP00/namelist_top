!!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!! NEMO/TOP1 :  1 - tracer definition                     (namtrc    )
!!              2 - tracer data initialisation            (namtrc_dta)
!!              3 - tracer advection                      (namtrc_adv)
!!              4 - tracer lateral diffusion              (namtrc_ldf)
!!              5 - tracer vertical physics               (namtrc_zdf)
!!              6 - tracer newtonian damping              (namtrc_dmp)
!!              7 - dynamical tracer trends               (namtrc_trd)
!!              8 - tracer output diagonstics             (namtrc_dia)
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
&namtrc     !   tracers definition
!,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
   nn_dttrc      =  1        !  time step frequency for passive sn_tracers      
   nn_writetrc   =  5475     !  time step frequency for sn_tracer outputs
   ln_rsttr      = .false.   !  start from a restart file (T) or not (F)
   nn_rsttr      =   0       !  restart control = 0 initial time step is not compared to the restart file value
                             !                  = 1 do not use the value in the restart file
                             !                  = 2 calendar parameters read in the restart file
   cn_trcrst_in  = "restart_trc_in"   !  suffix of pass. sn_tracer restart name (input)
   cn_trcrst_out = "restart_trc"      !  suffix of pass. sn_tracer restart name (output)
   ln_trcdta     =   .true.  !  Initialisation from data input file (T) or not (F)
   ln_trcdmp     =  .false.  !  add a damping termn (T) or not (F)
   ln_altres     = .true.   ! Ignore missing rs fields for passive tracers.
!
!                !    name   !           title of the field              ! initial data ! initial data ! save   !
!                !           !                                           !  units       ! from file    ! or not ! 
!                !           !                                           !              ! or not       !        !
   sn_tracer(1)   = 'DIC     ' , 'Dissolved inorganic Concentration CMOC ',  'mol-C/L' ,  .true.     ,  .true.
   sn_tracer(2)   = 'Alkalini' , 'Total Alkalinity Concentration CMOC    ',  'eq/L '   ,  .true.     ,  .true.
   sn_tracer(3)   = 'O2      ' , 'Dissolved Oxygen Concentration CMOC    ',  'mol-C/L' ,  .true.     ,  .true.
   sn_tracer(4)   = 'POC     ' , 'Detritus Concentration CMOC            ',  'mol-C/L' ,  .false.    ,  .true. 
   sn_tracer(5)   = 'PHY     ' , 'Nanophytoplankton Concentration CMOC   ',  'mol-C/L' ,  .false.    ,  .true. 
   sn_tracer(6)   = 'ZOO     ' , 'Microzooplankton Concentration CMOC    ',  'mol-C/L' ,  .false.    ,  .true. 
   sn_tracer(7)   = 'NCHL    ' , 'Nano chlorophyl Concentration CMOC     ',  'mol-C/L' ,  .false.    ,  .true. 
   sn_tracer(8)   = 'NO3     ' , 'Total Dissolved Nitrogen CMOC          ',  'mol-C/L' ,  .true.     ,  .true.  
   sn_tracer(9)   = 'DICabio ' , 'Abiotic DIC                            ',  'mol-C/L' ,  .true.     ,  .true.
   sn_tracer(10)  = 'O2abio  ' , 'Abiotic oxygen                         ',  'mol-C/L' ,  .true.     ,  .true.
   sn_tracer(11)  = 'DICnat  ' , 'Natural DIC                            ',  'mol-C/L' ,  .true.     ,  .true.
   sn_tracer(12)  = 'DI14C   ' , 'Radiocarbon DIC                        ',  'mol-C/L' ,  .true.     ,  .true.
   sn_tracer(13)  = 'agessc  ' , 'Ideal Age                              ',  'years' ,    .false.     ,  .true.  
/
!-----------------------------------------------------------------------
&namtrc_dta      !    Initialisation from data input file
!-----------------------------------------------------------------------
!
!                !  file name               ! frequency (hours) ! variable   ! time interp. !  clim  ! 'yearly'/ ! weights  ! rotation !
!                !                          !  (if <0  months)  !   name     !   (logical)  !  (T/F) ! 'monthly' ! filename ! pairing  !
   sn_trcdta(1)  = 'data_dic_nomask'        ,        -12        ,  'DIC'     ,    .false.   , .true. , 'yearly'  , ''       , ''
   sn_trcdta(2)  = 'data_alkalini_nomask'   ,        -12        ,  'Alkalini',    .false.   , .true. , 'yearly'  , ''       , ''
   sn_trcdta(3)  = 'data_o2_nomask'         ,        -12        ,  'O2'      ,    .false.   , .true. , 'yearly'  , ''       , ''
   sn_trcdta(8)  = 'data_no3_nomask'        ,        -12        ,  'NO3'     ,    .false.   , .true. , 'yearly'  , ''       , ''
   sn_trcdta(9)  = 'data_ct_abio_nomask'    ,        -12        ,  'DICabio' ,    .false.   , .true. , 'yearly'  , ''       , ''
   sn_trcdta(10) = 'data_o2abio_nomask'     ,        -12        ,  'O2abio'  ,    .false.   , .true. , 'yearly'  , ''       , ''
   sn_trcdta(11) = 'data_dic_nomask'        ,        -12        ,  'DIC'     ,    .false.   , .true. , 'yearly'  , ''       , ''
   sn_trcdta(12) = 'data_c14t_abio_nomask'  ,        -12        ,  'C14abio' ,    .false.   , .true. , 'yearly'  , ''       , ''
!
   cn_dir        =  './'      !  root directory for the location of the data files
   rn_trfac(1)   =   1.0e-06  !  multiplicative factor
   rn_trfac(2)   =   1.0e-06  !
   rn_trfac(3)   =  44.6e-06  !
   rn_trfac(8)   =  6.625e-06 !
   rn_trfac(9)   =   1.0e-06  ! 
   rn_trfac(10)  =  44.6e-06  !
   rn_trfac(11)  =   1.0e-06  ! 
   rn_trfac(12)  =   1.0e-06  ! 
/
!-----------------------------------------------------------------------
&namtrc_adv    !   advection scheme for passive tracer 
!-----------------------------------------------------------------------
   ln_trcadv_cen2   =  .false.  !  2nd order centered scheme   
   ln_trcadv_tvd    =  .false.  !  TVD scheme
   ln_trcadv_muscl  =  .true.   !  MUSCL scheme
   ln_trcadv_muscl2 =  .false.  !  MUSCL2 scheme + cen2 at boundaries
   ln_trcadv_ubs    =  .false.  !  UBS scheme
   ln_trcadv_qck    =  .false.  !  QUICKEST scheme
/
!-----------------------------------------------------------------------
&namtrc_ldf    !   lateral diffusion scheme for passive tracer 
!-----------------------------------------------------------------------
   ln_trcldf_diff   =  .true.   !  performs lateral diffusion (T) or not (F)
!                               !  Type of the operator : 
   ln_trcldf_lap    =  .true.   !     laplacian operator       
   ln_trcldf_bilap  =  .false.  !     bilaplacian operator     
                                !  Direction of action  :
   ln_trcldf_level  =  .false.  !     iso-level                
   ln_trcldf_hor    =  .false.  !     horizontal (geopotential)         (require "key_ldfslp" when ln_sco=T)
   ln_trcldf_iso    =  .true.   !     iso-neutral                       (require "key_ldfslp")
!                               !  Coefficient
   rn_ahtrc_0       =  1000.    !  horizontal eddy diffusivity for tracers [m2/s]
   rn_ahtrb_0       =     0.    !     background eddy diffusivity for ldf_iso [m2/s]
/
!-----------------------------------------------------------------------
&namtrc_zdf        !   vertical physics
!-----------------------------------------------------------------------
   ln_trczdf_exp   =  .false.  !  split explicit (T) or implicit (F) time stepping
   nn_trczdf_exp   =   3       !  number of sub-timestep for ln_trczdfexp=T
/
!-----------------------------------------------------------------------
&namtrc_rad        !  treatment of negative concentrations 
!-----------------------------------------------------------------------
   ln_trcrad   =  .true.  !  artificially correct negative concentrations (T) or not (F)
/
!-----------------------------------------------------------------------
&namtrc_dmp    !   passive tracer newtonian damping   
!-----------------------------------------------------------------------
   nn_hdmp_tr  =   -1      !  horizontal shape =-1, damping in Med and Red Seas only
                           !                   =XX, damping poleward of XX degrees (XX>0)
                           !                      + F(distance-to-coast) + Red and Med Seas
   nn_zdmp_tr  =    1      !  vertical   shape =0    damping throughout the water column
                           !                   =1 no damping in the mixing layer (kz  criteria)
                           !                   =2 no damping in the mixed  layer (rho crieria)
   rn_surf_tr  =   50.     !  surface time scale of damping   [days]
   rn_bot_tr   =  360.     !  bottom  time scale of damping   [days]
   rn_dep_tr   =  800.     !  depth of transition between rn_surf and rn_bot [meters]
   nn_file_tr  =    0      !  create a damping.coeff NetCDF file (=1) or not (=0)
/
!-----------------------------------------------------------------------
&namtrc_trd       !   diagnostics on tracer trends        ('key_trdtrc')
!                          or mixed-layer trends          ('key_trdmld_trc')
!----------------------------------------------------------------------
   nn_trd_trc  =  5475      !  time step frequency and tracers trends
   nn_ctls_trc =   0        !  control surface type in mixed-layer trends (0,1 or n<jpk)
   rn_ucf_trc  =   1        !  unit conversion factor (=1 -> /seconds ; =86400. -> /day)
   ln_trdmld_trc_restart = .false.  !  restart for ML diagnostics
   ln_trdmld_trc_instant = .true.  !  flag to diagnose trends of instantantaneous or mean ML T/S
   ln_trdtrc(1)  =   .true.
   ln_trdtrc(2)  =   .true.
   ln_trdtrc(8) =   .true. ! <CMOC OR 07/15/2014> ! Removal of all the tracers !  
/
!-----------------------------------------------------------------------
&namtrc_dia       !   parameters for passive tracer additional diagnostics
!----------------------------------------------------------------------
   ln_diatrc     =  .true.   !  save additional diag. (T) or not (F)
   nn_writedia   =  5475     !  time step frequency for diagnostics
/
