#=============================================================
#
#                  CanNEMO 
#       A maestro suite for ocean only runs
#
# Enable cmip6_diag option -- D.YANG, OCT/2017
# with_nemo_diag=on: the cmip6 diagnostics (default);
# with_nemo_diag=off: the CCCma default output 
#
#==============================================================

#-----------------------------------------------
# USER SETTINGS
#
# This section replaces the old "makefiles" and
# can be modified as needed by users. After 
# editing  you must run bin/config-maestro to apply 
# the setting and before submitting the run. 
#-----------------------------------------------

#==================================================================================================
#  Basic run settings
#==================================================================================================
# Used to identify the run. Often a 3-letter "official" CCCma runid.
RUNID=st7

# Name of the configuration that the experiment is based on (e.g. CCC_ORCA1_LIM)
config=
 
# The start date of the run. Format is: YYYY:MM
start=001:01

# End date of the run. Format is: YYYY:MM
end=0010:12

# CMIP6 post-processing switch
with_nemo_diag=off

# The "chunk-size" of the run in months or years. Format nnM or nnY.
# This is how long the model will be run for in each chunk. The model
# must be able to complete this in <3hr.
nemo_freq=5y

# This is the amount of time that the "nemo_run" node will be assigned (in min).
# The longer "nemo_freq" or the higher the resolution, the more time
# will be required. The max is 180 min.
nemo_wallclock=180

# The hall to run on (banting/ppp3=hall3 or daley/ppp4=hall4). This will be used
# to update resources/resources.def when calling config-maestro, and will also
# be used to define "OUTPUT_PATH", which is where the data will end up.
hall=hall3

# compilation options
PLATFORM="eccc-xc50"
COMPILER="intel"
add_cpp_keys=""
del_cpp_keys=""
if [[ "$with_nemo_diag" == "on" ]]; then
    add_cpp_keys+=" key_trdtra key_diaar5"
fi

# The NEMO output fields are organized in the following 8 priority levels. 
# level 0: CCCma default output fields, some of which also belong to 
#          the CMIP6 priority level 1; 
# level 1~3: the CMIP6 priority level 1~3 fields; 
# level 4: cfc11, cfc12 & sf6, which are CMIP6 priority level 1 or 2,
#          but only make sense for the historical experiments. 
# level 5~7: levels saved for future use. 
# level 8: CCCma internal used fields for development. 
# output_level defines the priority levels that are actually output. 
# For example: output_level=4 means to output the priority levels 4, 3, 2, 1 and 0.
output_level=3
if [ $with_nemo_diag = off ] ; then
  output_level=0
fi

# nemo_trc = off means use physical ocean only
# nemo_trc = on  means use passive tracers (general, CFC, MY_TRC, even if pisces/cmoc=off)
nemo_trc=on

# nemo_carbon = off means no carbon tracer only
# nemo_carbon = on  means include PISCES/CMOC/CanOE etc
nemo_carbon=off

# nemo_cmoc = off for PISCES or CanOE
# nemo_cmoc = on for CMOC
nemo_cmoc=off

# pisces_offline = off means use online model
# pisces_offline = on  means use offline model with prescribed velocities
pisces_offline=off

## CFC section
nemo_cfc=off # toggle CFC and SF6 simulation 'on' or 'off'
offset_cfc_year=0 # How much to offset the CFCs by, for example if this value is 1900 and
                       # start set above is set to 001, the year that the cfc module uses to 
                       # calculate the atmospheric concentration is 1900+001=1901

# Cleanup runtime directories - what used to be tmp directories (opposite of debug). 1=yes, 0=no.
# Note: to find where run is occuring, look in $SEQ_EXP_HOME/hub/mach
# where "mach" is brooks or hare. This will be the place you specified as scratch in 
# "$HOME/.suites/.default_links"
NEMO_CLEAN_TMP=on
# Number of previous "tmp" directories to keep on disk. For example, if NEMO_CLEANUP_LAG=3,
# the last 3 iterations through the loops will be kept on disk but older iterations will be deleted.
NEMO_CLEANUP_LAG=3

# Dump to tape settings
# -If nemo_dump=on, then maestro will automatically dump  history files with the suffixes
#   and time avg'd values in nemo_dump_file_suff/_freq, respectively. 
# -If, additionally, nemo_dump_rs=on, then restart files will also be dumped.
# -Note that nemo_DumpDel.tsk steps through the lists nemo_dump_file_suff/_freq at the same time, 
#   so these arrays must be the same size else nemo_DumpDel.tsk will bail.
# -If archiving is used, the files are archived to the project defined by nemo_arch_proj, with the individual archive
#   name being runid_nemochunkstartyear_nemochunkendyear_dateofarchiving.
nemo_dump=on
nemo_arch_proj="crd_short_term"  
nemo_dump_file_suff='grid_T grid_U  grid_V  grid_W  icemod'
nemo_dump_file_freq='1m     1m      1m      1m      1m    '
if [ ${nemo_carbon} = on -a ${pisces_offline} = off ] ; then 
  nemo_dump_file_suff="${nemo_dump_file_suff} ptrc_T diad_T"
  nemo_dump_file_freq="${nemo_dump_file_freq}       1m       1m"
fi
if [ ${with_nemo_diag} = on -a ${output_level} -ge 1 ] ; then          # CMIP6
  nemo_dump_file_suff="${nemo_dump_file_suff} grid_T_ar6 grid_U_ar6 grid_V_ar6 grid_W_ar6 icemod grid_T_ar6 grid_U_ar6 grid_V_ar6 diaptr scalar_ar6 grid_T_ar6"
  nemo_dump_file_freq="${nemo_dump_file_freq}       1m       1m       1m       1m       1d       1d       1d       1d       1d       1m       1y"
fi
nemo_dump_rs=on

# -If nemo_del=on, then maestro will automatically delete the files history files with the suffixes 
#    and time avg values in nemo_del_file_suff and nemo_del_file_freq, respectively. 
# -Restart files will also be deleted in nemo_del_rs=on 
# -Note that nemo_DumpDel.tsk steps through the lists nemo_del_file_suff/_freq at the same time, 
#   so these arrays must be the same size else nemo_DumpDel.tsk will bail. 
# -NOTE: IT IS POSSIBLE TO DELETE FILES FROM SITESTORE EVEN IF THEY WEREN'T ARCHIVED. This can be achieved 
#   if nemo_dump=off, and nemo_del=on. BE CERTAIN THIS IS WHAT YOU WANT.
nemo_del=on
nemo_del_file_suff='grid_T grid_U  grid_V  grid_W  icemod'
nemo_del_file_freq='1m     1m      1m      1m      1m    '
if [ ${nemo_carbon} = on -a ${pisces_offline} = off ] ; then
  nemo_del_file_suff="${nemo_del_file_suff} ptrc_T diad_T"
  nemo_del_file_freq="${nemo_del_file_freq}     1m     1m"
fi
if [ ${with_nemo_diag} = on -a ${output_level} -ge 1 ] ; then          # CMIP6
  nemo_del_file_suff="${nemo_del_file_suff} grid_T_ar6 grid_U_ar6 grid_V_ar6 grid_W_ar6 icemod grid_T_ar6 grid_U_ar6 grid_V_ar6 diaptr scalar_ar6 grid_T_ar6"
  nemo_del_file_freq="${nemo_del_file_freq}      1m       1m       1m       1m       1d       1d       1d       1d       1d       1m       1y"
fi
nemo_del_rs=on

# RTD settings
# If nemo_rtd is true then execute the nemo run time diagnostics
nemo_rtd=on

# RTDs are now saved in the RS tarfile. If nemo_rtd_from_rs=on, we will extract the rtd from the
# rs tar file and append to it in this run (if nemo_rs is defined). If you want to start a "fresh"
# RTD then set this to off.
nemo_rtd_from_rs=off

# NemoVal Settings
# If nemoval_diag is on then create time averaged 2D plots and show them at http://wiki.cccma.ec.gc.ca/NemoVal/
# nemoval_root points to the location of pertinent files on sitestore; this value must be consistent with the 
# NVAL_ROOT value used by the web-app (see /var/www/cccwebapp/NemoVal/NViewConfig.sh on wiki)
nemoval_diag=on
nemoval_root='/home/ords/crd/ccrn/scrd104/nemoval'

# nemoval_pltyml should be a yaml file that defines the desired NemoVal plots, and nemoval_pltyml_pth 
# should be the absolute path (on the sci-network) for this file. If no yaml file is provided here, yet
# nemoval_diag=on, a default yaml, according to the type of run (i.e. a cmoc run, or canoe run), will be used. 
# The defaults can be found here $nemoval_root/data/maestro/def_yamls.
nemoval_pltyml=
nemoval_pltyml_pth=

# Defines what resolution of mask to use for NemoVal plots (must match the resolution of model). 
# Value is only used to choose mask file; if a standard config file was set in the setup-nemo call, this gets set according 
# to what file was used. If a custom config file is used, this must be set manually if the model resolution differs from the default value.
# Currently supported options: 1d (1 degree), 2d (2 degree), 025d (quarter degree)
nemoval_res=1d

# Defines at what frequency NemoVal plots should be produced (nemoval_repeat_period), 
# and what duration the plots should be avg'd over (nemoval_avg_period). IN YEARS.
#   Ex: with a repeat period of 100 years, and avg period of 10 years, at the end of year
#   100, data from years 91-100 will be used to produce the plots and this will be repeated every 
#   100 years of simulation (i.e. 191-200, 291-300, etc..)
# NOTES:
#   - plots will be activated according to the four digit year identifier, as opposed to the 
#       number of simulation years. This means that if nemoval_repeat_period=100, yet the simulation
#       started at year 50, plots will still be activated at the end of year 0100.
nemoval_avg_period=10
nemoval_repeat_period=100

# nemo_from_rest is used to flag starting nemo from rest rather than a restart
# If this is set "on" then the user must also supply namelist files via the
# variables nemo_namelist and nemo_namelist_ice
nemo_from_rest=on

#==================================================================================================
#  Input file section
#==================================================================================================

# The name of a tar file containing the NEMO restart files
# This must be set for the first job of a run unless nemo_from_rest = on
# nemo_restart should be unset or null otherwise.
# **A full path can and should be specified.**
nemo_restart=''


# executables
nemo_exec=nemo.exe
nemo_diag_exe=nemo_diag.exe
nemo_carbon_diag_exe=nemo_diag_cmoc.exe
physical_rtd_exe=nemo_physical_rtd.exe
ice_rtd_exe=nemo_ice_rtd.exe
carbon_rtd_exe=nemo_carbon-cmoc_rtd.exe

# File containing grid information
orca_grid_info=nemo3.4.1_orca1_mesh_mask_ngr.nc

# Namelist like files
nemo_namelist=namelist_in
nemo_namelist_ice=namelist_ice_in
nemo_namelist_top=namelist_top_in
nemo_namelist_pisces=
nemo_namelist_cmoc=
nemo_iodef=iodef.xml
nemo_xmlio_server_def=xmlio_server.def

# General init files
nemo_ahmcoef=nemo_3.4_orca1_ahmcoef
nemo_data_1m_potential_temperature_nomask=omip6_orca1_data_ptmp_nomask.nc
nemo_data_1m_salinity_nomask=omip6_orca1_data_salt_nomask.nc
nemo_coordinates=nemo_3.4_orca1_coordinates_ukorca1.nc
nemo_bathy_meter=nemo_3.4_orca1_bathy_meter_050308_ukmo_mod.nc
nemo_mask_itf=nemo_3.4_orca1_mask_itf_orca1_new.nc
nemo_M2rowdrg=nemo_3.4_orca1_m2rowdrg_r1_modif.nc
nemo_K1rowdrg=nemo_3.4_orca1_k1rowdrg_r1_modif.nc
nemo_Eddyengf=nemo_3.4_orca1_eddy_mix.nc
nemo_geothermal_heating=nemo_3.3_orca2_geothermal_heating.nc
nemo_sss_data=omip6_orca1_data_sss_nomask.nc
nemo_sst_data=nemo_3.4_orca1_sst_1m_orca1.nc
nemo_chlorophyll=nemo_3.4_orca1_chlorophyll-orca1_1m.nc
nemo_subbasins=nemo_3.4_orca1_basinmask_050308_ukmo_modified.nc

# Default weight files set for ORCA1 resolution
nemo_weights_bicubic2=uncs_orca1_canesm2_weights_bicubic.nc # weight for CanESM2 bulk mode
nemo_weights_bilinear2=uncs_orca1_canesm2_weights_bilin.nc  # weight for CanESM2 bulk mode
nemo_weights_bicubic3=uncs_orca1_canesm2_weights_bicubic.nc # weight for CanESM2 flux mode
nemo_weights_bilinear3=uncs_orca1_canesm2_weights_bilin.nc  # weight for CanESM2 flux mode

#carbon init / surface forcing files
#general
nemo_presatm=uncs_orca1_presatm.nc
nemo_dust_orca=uncs_orca1_dust.orca.nc
nemo_river_orca=nemo_3.4_orca2_river.orca.nc
nemo_ndeposition_orca=uncs_orca1_ndeposition.orca.nc
nemo_bathy_orca=uncs_orca1_bathy.orca.nc

nemo_fermask_orca=nemo_3.4_orca1_femask.orca.nc # CMOC only

nemo_data_dic_nomask=omip6_orca1_data_ct_preind_nomask.nc
nemo_data_alkalini_nomask=omip6_orca1_data_at_nomask.nc
nemo_data_o2_nomask=omip6_orca1_data_o2_nomask.nc
nemo_data_po4_nomask=omip6_orca1_data_po4_nomask.nc
nemo_data_si_nomask=omip6_orca1_data_si_nomask.nc
nemo_data_no3_nomask=omip6_orca1_data_no3_nomask.nc
nemo_data_doc_nomask=urjc_orca1_data_doc_nomask.nc
nemo_data_fer_nomask=urjc_orca1_data_fer_nomask.nc
nemo_data_atcco2=omip6_co2atm.nc
nemo_data_atc14c=omip6_Delta14co2.nc
# Abiotic BGC
nemo_data_at_abio_nomask=omip6_orca1_data_at_abio_nomask.nc
nemo_data_c14t_abio_nomask=omip6_orca1_data_c14t_abio_nomask.nc
nemo_data_ct_abio_nomask=omip6_orca1_data_ct_abio_nomask.nc
nemo_data_o2_abio_nomask=omip6_orca1_data_o2abio_nomask.nc

#cfc atmospheric history
nemo_cfc_atmhist=ras003_nemo_cfc_atmhist.nc

# Dynamics files for running offline
dyna_grid_t=
dyna_grid_u=
dyna_grid_v=
dyna_grid_w=
offline_mash_mask=

##########################################################################################################################
#   Surface forcing options
#
#   XNEMO SUPPORT FOR FORCING OPTIONS
#   ---------------------------------
#   The nemo_forcing variable specifies whether the files for "bulk" or "flux" forcing are read. The "_iaf" option (i.e.
#   bulk_iaf, flux_iaf), allows the user to specify a filename prefix, and XNEMO will intelligently append the 
#   "_yYYYY.nc" suffix. The four digit year, YYYY, is controlled by the current nemo year (starting in "start"). The
#   variable "iaf_year_offset" can be used if the model year is different from the desired forcing year. iaf_year_offset gives 
#   the offset between the current nemo year and the desired year in the forcing dataset such that: 
#
#         forcing_year = current_year + iaf_year_offset.
#
#   The data to be used will end in _y${forcing_year}.nc. # For interannual forcing, "iaf_loop_year" gives the year at which the
#   forcing will loop back to the first specified year of forcing. The number is given in terms of "forcing_year" as opposed to
#   nemo "current_year": forcing_year = current_year + iaf_year_offset. Using "iaf_loop_year", a user can repeat forcing for
#   several cycles. A cycle could be as short as 1 year (i.e. climatological forcing).
#
#   XNEMO also now has the ability to modify namelist parameters relevant to the forcing. Thus, instead of manually changing the
#   namelist and resaving it (e.g. to change from bulk to flux forcing), users can simply specify the required changes in their
#   makefile (see examples below).    
#
#
#   ACTUAL FORCING AVAILABLE ON DISK
#   --------------------------------
#
#   The default surface forcing for ocean-only mode is the COREv2 normal year forcing (bulk). This is the 
#   "official" forcing which is tested most. As a courtesy, some examples are provided below for other forcing that
#   has been prepared and made available on disk. Changing the forcing requires setting various run parameters (start, end
#   rtd_diag_start, iaf_year_offset, iaf_loop_year), as well as namelist parameters, and possibly changing compilation 
#   options, weights files etc. Sensible reccommendations are made below, for various types of forcing. 
#
#   *NOTE*: It is always the users responsibility to make sure that a run is setup according to their needs. The are no
#           guarantees whatsoever as to the suitability or usability of the forcings or setups below.
#
#                         --------------------------------------------------------------    
#
#                             BELOW, COMMENT/UNCOMMENT FORCING YOU WISH TO USE
#       
##########################################################################################################################
#    COREv2 normal year forcing (climatogical bulk forcing)
##########################################################################################################################
# 
# #      # Sensible variable options (see definitions at top)
#       nemo_forcing=bulk
#       year_rtdiag_start=001
#       iaf_year_offset=0     
#       iaf_loop_year=0
#       ln_co2int=.false. # do not read variable CO2 from a file
# 
#       # namelist paramters to be changed by XNEMO
#       sn_wndi=" 'uwnd10m'     ,         6         , 'U_10_MOD',   .false.    , .true. , 'yearly'  , 'weights_bic2' , 'Uwnd' "
#       sn_wndj=" 'vwnd10m'     ,         6         , 'V_10_MOD',   .false.    , .true. , 'yearly'  , 'weights_bic2' , 'Vwnd' "
#       sn_qsr=" 'qsw'         ,        24         , 'SWDN_MOD',   .false.    , .true. , 'yearly'  , 'weights_bil2' , '' "
#       sn_qlw=" 'qlw'         ,        24         , 'LWDN_MOD',   .false.    , .true. , 'yearly'  , 'weights_bil2' , '' "
#       sn_tair=" 'tair10m'     ,         6         , 'T_10_MOD',   .false.    , .true. , 'yearly'  , 'weights_bil2' , '' "
#       sn_humi=" 'humi10m'     ,         6         , 'Q_10_MOD',   .false.    , .true. , 'yearly'  , 'weights_bil2' , '' "
#       sn_prec=" 'precip'      ,        -1         , 'PRC_MOD'    ,   .false.    , .true. , 'yearly'  , 'weights_bil2' , '' "
#       sn_snow=" 'snow'        ,        -1         , 'SNOW'    ,   .false.    , .true. , 'yearly'  , 'weights_bil2' , '' "
#       sn_tdif=" 'taudif_core' ,        24         , 'taudif'  ,   .false.    , .true. , 'yearly'  , ''             , '' "
#       
#       # Forcing files to be accessed
#       nemo_t=uncs_corev2-ny_t_10.15JUNE2009.nc
#       nemo_u=uncs_corev2-ny_u_10.15JUNE2009.nc
#       nemo_v=uncs_corev2-ny_v_10.15JUNE2009.nc
#       nemo_q=uncs_corev2-ny_q_10.15JUNE2009.nc
#       nemo_qsw=uncs_corev2-ny_ncar_rad.15JUNE2009.nc
#       nemo_qlw=uncs_corev2-ny_ncar_rad.15JUNE2009.nc
#       nemo_precip=uncs_corev2-ny_ncar_precip.15JUNE2009.nc
#       nemo_snow=uncs_corev2-ny_ncar_precip.15JUNE2009.nc
# 
#       # WEIGHTS file for this resolution
#       nemo_weights_bicubic2=uncs_orca1_corev2_weights_bicubic.nc
#       nemo_weights_bilinear2=uncs_orca1_corev2_weights_bilin.nc
#
#     # Runoff to use
#     nemo_runoff_core_monthly=nemo_3.4_orca1_runoff_1m_orca1.nc
#     sn_rnf=" 'runoff_core_monthly',        -1         , 'sorunoff'    ,   .true.     , .true. , 'yearly'  , '', '' "
#     sn_cnf=" 'runoff_core_monthly',        -1         , 'socoeff'     ,   .true.     , .true. , 'yearly'  , '', '' "
#
###########################################################################################################################
##    COREv2 interannual forcing (1949-2009)
###########################################################################################################################
#      # Sensible variable options (see definitions at top)
#      nemo_forcing=bulk_iaf
##      The below setting correspond to a "start" (above) of 1. Adjust iaf_year_offset accordingly for a different start.
#      year_rtdiag_start=001
#      iaf_year_offset=1948     
#      iaf_loop_year=2009
#      ln_co2int=.false. # do not read variable CO2 from a file
#
#      # namelist paramters to be changed by XNEMO
#      sn_wndi=" 'uwnd10m'     ,         6         , 'U_10_MOD',   .true.    , .false. , 'yearly'  , '' , 'Uwnd' "
#      sn_wndj=" 'vwnd10m'     ,         6         , 'V_10_MOD',   .true.    , .false. , 'yearly'  , '' , 'Vwnd' "
#      sn_qsr="  'qsw'         ,        24         , 'SWDN_MOD',   .true.    , .false. , 'yearly'  , '' , '' "
#      sn_qlw="  'qlw'         ,        24         , 'LWDN_MOD',   .true.    , .false. , 'yearly'  , '' , '' "
#      sn_tair=" 'tair10m'     ,         6         , 'T_10_MOD',   .true.    , .false. , 'yearly'  , '' , '' "
#      sn_humi=" 'humi10m'     ,         6         , 'Q_10_MOD',   .true.    , .false. , 'yearly'  , '' , '' "
#      sn_prec=" 'precip'      ,        -1         , 'PRC_MOD' ,   .true.    , .false. , 'yearly'  , '' , '' "
#      sn_snow=" 'snow'        ,        -1         , 'SNOW'    ,   .true.    , .false. , 'yearly'  , '' , '' "
#      sn_tdif=" 'taudif_core' ,        24         , 'taudif'  ,   .false.    , .false. , 'yearly'  , '' , '' "
#
#      # Forcing files to be accessed
#      # In the case of interannual forcing, just give the file prefix. The ${prefix}_yYYYY.nc will be added
#      # by xnemo.
#      nemo_t=omip6_orca1_corev2_t_10
#      nemo_u=omip6_orca1_corev2_u_10
#      nemo_v=omip6_orca1_corev2_v_10
#      nemo_q=omip6_orca1_corev2_q_10
#      nemo_qsw=omip6_orca1_corev2_ncar_rad
#      nemo_qlw=omip6_orca1_corev2_ncar_rad
#      nemo_precip=omip6_orca1_corev2_ncar_precip
#      nemo_snow=omip6_orca1_corev2_ncar_precip
#
#      # WEIGHTS file for this resolution
#      nemo_weights_bicubic2=
#      nemo_weights_bilinear2=
#
#     # Runoff to use
#     nemo_runoff_core_monthly=nemo_3.4_orca1_runoff_1m_orca1.nc
#     sn_rnf=" 'runoff_core_monthly',        -1         , 'sorunoff'    ,   .true.     , .true. , 'yearly'  , '', '' "
#     sn_cnf=" 'runoff_core_monthly',        -1         , 'socoeff'     ,   .true.     , .true. , 'yearly'  , '', '' "
#
#
###########################################################################################################################
##    CanESM2 climatogical flux forcing, from pre-industriual control run "iga" years 2201
#
#     NOTE: must use del_key="key_lim2" when compiling the code
###########################################################################################################################
#      # Sensible variable options (see definitions at top)
#      nemo_forcing=flux
##      The below setting correspond to a "start" (above) of 1. Adjust iaf_year_offset accordingly for a different start.
#      year_rtdiag_start=001
#      iaf_year_offset=0
#      iaf_loop_year=0
#      nn_ice=0 # no sea-ice for flux mode
#
#      # namelist paramters to be changed by XNEMO
#      sn_utau=" 'utau'      ,        24         , 'OUFS'    , .false.      , .false., 'yearly'  , 'weights_bic3', 'Uwnd' "
#      sn_vtau=" 'vtau'      ,        24         , 'OVFS'    , .false.      , .false., 'yearly'  , 'weights_bic3', 'Vwnd' "
#      sn_qtot=" 'qtot'      ,        24         , 'OBEG'    , .false.      , .false., 'yearly'  , 'weights_bil3', '' "
#      sn_qsr=" 'qsr'       ,        24         , 'OFSG'    , .false.      , .false., 'yearly'  , 'weights_bil3', '' "
#      sn_emp=" 'emp'       ,        24         , 'OBWG'    , .false.      , .false., 'yearly'  , 'weights_bil3', '' "
#
#      nemo_utau=uncs_oufs_iga_y2201.nc
#      nemo_vtau=uncs_ovfs_iga_y2201.nc
#      nemo_qtot=uncs_obeg_iga_y2201.nc
#      nemo_qsr=uncs_ofsg_iga_y2201.nc
#      nemo_emp=uncs_obwg_iga_y2201.nc
#
#      # Runoff to use
#      nemo_runoff_core_monthly=nemo_3.4_orca1_rout_socoeff_month_r1i1p1_1979-2005-mean.nc
#      sn_rnf=" 'runoff_core_monthly',        -1         , 'ROUT'    ,   .true.     , .true. , 'yearly'  , 'weights_bil3', '' "
#      sn_cnf=" 'runoff_core_monthly',        -1         , 'socoeff' ,   .true.     , .true. , 'yearly'  , 'weights_bil3', '' "
#
###########################################################################################################################
##    CanESM2 interannual flux forcing, from pre-industriual control run "iga" years 2201-2299
#
#     NOTE: must use del_key="key_lim2" when compiling the code
###########################################################################################################################
#      # Sensible variable options (see definitions at top)
#      nemo_forcing=flux_iaf
# #      The below setting correspond to a "start" (above) of 1. Adjust iaf_year_offset accordingly for a different start.
#      year_rtdiag_start=001
#      iaf_year_offset=2199    
#      iaf_loop_year=2299
#      nn_ice=0 # no sea-ice for flux mode
# 
#      # namelist paramters to be changed by XNEMO
#      sn_utau=" 'utau'      ,        24         , 'OUFS'    , .false.      , .false., 'yearly'  , 'weights_bic3', 'Uwnd' "
#      sn_vtau=" 'vtau'      ,        24         , 'OVFS'    , .false.      , .false., 'yearly'  , 'weights_bic3', 'Vwnd' "
#      sn_qtot=" 'qtot'      ,        24         , 'OBEG'    , .false.      , .false., 'yearly'  , 'weights_bil3', '' "
#      sn_qsr=" 'qsr'       ,        24         , 'OFSG'    , .false.      , .false., 'yearly'  , 'weights_bil3', '' "
#      sn_emp=" 'emp'       ,        24         , 'OBWG'    , .false.      , .false., 'yearly'  , 'weights_bil3', '' "
# 
#      # Forcing files to be accessed
#      # In the case of interannual forcing, just give the file prefix. The ${prefix}_yYYYY.nc will be added
#      # by xnemo.
#      nemo_utau=uncs_oufs_iga
#      nemo_vtau=uncs_ovfs_iga
#      nemo_qtot=uncs_obeg_iga
#      nemo_qsr=uncs_ofsg_iga
#      nemo_emp=uncs_obwg_iga
# 
#      # Runoff to use
#      nemo_runoff_core_monthly=nemo_3.4_orca1_rout_socoeff_month_r1i1p1_1979-2005-mean.nc
#      sn_rnf=" 'runoff_core_monthly',        -1         , 'ROUT'    ,   .true.     , .true. , 'yearly'  , 'weights_bil3', '' "
#      sn_cnf=" 'runoff_core_monthly',        -1         , 'socoeff' ,   .true.     , .true. , 'yearly'  , 'weights_bil3', '' "
#
###########################################################################################################################
##    CanESM2 interannual bulk forcing, from pre-industriual control run "iga" years 2201-2299
###########################################################################################################################
     # Sensible variable options (see definitions at top)
     nemo_forcing=bulk_iaf
#      The below setting correspond to a "start" (above) of 1. Adjust iaf_year_offset accordingly for a different start.
     year_rtdiag_start=001
     iaf_year_offset=2199     
     iaf_loop_year=2299
     ln_co2int=.false. # do not read variable CO2 from a file

     # namelist paramters to be changed by XNEMO
      sn_wndi=" 'uwnd10m'      ,        24         , 'uas'     ,   .false.    , .false. , 'yearly'  , 'weights_bic2'       , 'Uwnd' "
      sn_wndj=" 'vwnd10m'      ,        24         , 'vas'     ,   .false.    , .false. , 'yearly'  , 'weights_bic2'       , 'Vwnd' "
      sn_qsr=" 'qsw'          ,        24         , 'rsds'    ,   .false.    , .false. , 'yearly'  , 'weights_bil2'       , '' "
      sn_qlw=" 'qlw'          ,        24         , 'rlds'    ,   .false.    , .false. , 'yearly'  , 'weights_bil2'       , '' "
      sn_tair=" 'tair10m'      ,        24         , 'tas'     ,   .false.    , .false. , 'yearly'  , 'weights_bil2'       , '' "
      sn_humi=" 'humi10m'      ,        24         , 'huss'    ,   .false.    , .false. , 'yearly'  , 'weights_bil2'       , '' "
      sn_prec=" 'precip'       ,        24         , 'pr'      ,   .false.    , .false. , 'yearly'  , 'weights_bil2'       , '' "
      sn_snow=" 'snow'         ,        24         , 'prsn'    ,   .false.    , .false. , 'yearly'  , 'weights_bil2'       , '' "
      sn_tdif=" 'taudif_core'  ,        24         , 'taudif'  ,   .false.    , .true. , 'yearly'  , ''       , '' "

     # In the case of interannual forcing, just give the file prefix. The ${prefix}_yYYYY.nc will be added
     # by xnemo.
     nemo_t=uncs_tas_iga
     nemo_u=uncs_uas_iga
     nemo_v=uncs_vas_iga
     nemo_q=uncs_huss_iga
     nemo_qsw=uncs_rsds_iga
     nemo_qlw=uncs_rlds_iga
     nemo_precip=uncs_pr_iga
     nemo_snow=uncs_prsn_iga

     # Runoff to use
     nemo_runoff_core_monthly=nemo_3.4_orca1_rout_socoeff_month_r1i1p1_1979-2005-mean.nc
     sn_rnf=" 'runoff_core_monthly',        -1         , 'ROUT'    ,   .true.     , .true. , 'yearly'  , 'weights_bil2', '' "
     sn_cnf=" 'runoff_core_monthly',        -1         , 'socoeff' ,   .true.     , .true. , 'yearly'  , 'weights_bil2', '' "

###########################################################################################################################
##    CanESM2 interannual bulk forcing, from historical run "igm" years 1850-2005
###########################################################################################################################
#      # Sensible variable options (see definitions at top)
#      nemo_forcing=bulk_iaf
##      The below setting correspond to a "start" (above) of 1850. Adjust iaf_year_offset accordingly for a different start.
#      year_rtdiag_start=1850
#      iaf_year_offset=0   
#      iaf_loop_year=2005
#      ln_co2int=.true. # variable CO2 read from file
#
#      # namelist paramters to be changed by XNEMO
#       sn_wndi=" 'uwnd10m'      ,        24         , 'uas'     ,   .false.    , .false. , 'yearly'  , 'weights_bic2'       , 'Uwnd' "
#       sn_wndj=" 'vwnd10m'      ,        24         , 'vas'     ,   .false.    , .false. , 'yearly'  , 'weights_bic2'       , 'Vwnd' "
#       sn_qsr=" 'qsw'          ,        24         , 'rsds'    ,   .false.    , .false. , 'yearly'  , 'weights_bil2'       , '' "
#       sn_qlw=" 'qlw'          ,        24         , 'rlds'    ,   .false.    , .false. , 'yearly'  , 'weights_bil2'       , '' "
#       sn_tair=" 'tair10m'      ,        24         , 'tas'     ,   .false.    , .false. , 'yearly'  , 'weights_bil2'       , '' "
#       sn_humi=" 'humi10m'      ,        24         , 'huss'    ,   .false.    , .false. , 'yearly'  , 'weights_bil2'       , '' "
#       sn_prec=" 'precip'       ,        24         , 'pr'      ,   .false.    , .false. , 'yearly'  , 'weights_bil2'       , '' "
#       sn_snow=" 'snow'         ,        24         , 'prsn'    ,   .false.    , .false. , 'yearly'  , 'weights_bil2'       , '' "
#       sn_tdif=" 'taudif_core'  ,        24         , 'taudif'  ,   .false.    , .true. , 'yearly'  , ''       , '' "
#
#      # Forcing files to be accessed
#  # In the case of interannual forcing, just give the file prefix. The ${prefix}_yYYYY.nc will be added
#  # by xnemo.
#     nemo_t=uncs_tas_canesm2-igm
#     nemo_u=uncs_uas_canesm2-igm
#     nemo_v=uncs_vas_canesm2-igm
#     nemo_q=uncs_huss_canesm2-igm
#     nemo_qsw=uncs_rsds_canesm2-igm
#     nemo_qlw=uncs_rlds_canesm2-igm
#     nemo_precip=uncs_pr_canesm2-igm
#     nemo_snow=uncs_prsn_canesm2-igm
#
#     # Runoff to use
#     nemo_runoff_core_monthly=nemo_3.4_orca1_rout_socoeff_month_r1i1p1_1979-2005-mean.nc
#     sn_rnf=" 'runoff_core_monthly',        -1         , 'ROUT'    ,   .true.     , .true. , 'yearly'  , 'weights_bil2', '' "
#     sn_cnf=" 'runoff_core_monthly',        -1         , 'socoeff' ,   .true.     , .true. , 'yearly'  , 'weights_bil2', '' "
###########################################################################################################################

#================================
#   SAVING OUTPUT SECTION
#================================

  # Controls whether files will be moved to sitestore or not.
  nemo_save_hist=on

  # nemo_hist_file_suffix_list is a list of history file suffixes to to rebuild and save. The nemo_hist_file_freq_list
  # is the frequency associated with each file. The number of elements in these two arrays must be the same. If for
  # example you want grid_t at 1m and 1d frequency, then nemo_hist_file_suffix_list="grid_t grid_t" and nemo_hist_file_freq_list="1m 1d". 
  nemo_hist_file_suffix_list="grid_T grid_U grid_V grid_W icemod"
  nemo_hist_file_freq_list="      1m     1m     1m     1m    1m"
  if [ ${nemo_carbon} = on -a ${pisces_offline} = off ] ; then 
    nemo_hist_file_suffix_list="${nemo_hist_file_suffix_list} ptrc_T diad_T"
    nemo_hist_file_freq_list="${nemo_hist_file_freq_list}       1m       1m"
  fi
  if [ ${with_nemo_diag} = on -a ${output_level} -ge 1 ] ; then          # CMIP6 output
    nemo_hist_file_suffix_list="${nemo_hist_file_suffix_list} grid_T_ar6 grid_U_ar6 grid_V_ar6 grid_W_ar6 icemod grid_T_ar6 grid_U_ar6 grid_V_ar6 grid_T_ar6"
    nemo_hist_file_freq_list="${nemo_hist_file_freq_list}     1m       1m       1m       1m       1d       1d       1d       1d       1y"
  fi

#==================================================================================================
#  Namelist modification section
#==================================================================================================
 # Things specified below typically be written into the namelist at runtime, modifying it.

 # Normally user suppled namelist files are only required when starting the model
 # from rest. Otherwise the namelist files are stored in the restart archive and
 # read from the archive at each restart. Setting nemo_force_namelist = on will
 # over ride this behaviour and allow the namelists defined by the variables
 # nemo_namelist and nemo_namelist_ice to be used at the start of each new string.
 nemo_force_namelists=on

 # nn_ice =0 no ice boundary condition
 #        =1 use observed ice-cover
 #        =2 lim2 ice-model used
 #        =3 lim3 ice-model used
 #        =4 cice ice-model used
 nn_ice=2

#
runid=$RUNID 

#nemo time step in seconds
nemo_rdt=3600 
rn_rdt=${nemo_rdt}

 # Always use nn_it000 and nn_date0 from the namelist file
 #   nn_rstctl is restart control; activated only if ln_rstart = T
 #     = 0 nn_date0 read in namelist ; nn_it000 : read in namelist
 #     = 1 nn_date0 read in namelist ; nn_it000 : check consistancy between namelist and restart
 #     = 2 nn_date0 read in restart  ; nn_it000 : check consistancy between namelist and restart
 nn_rstctl=0

 # Leap year calendar (1) or not (0)
 nn_leapy=0

 # output the initial state (1) or not (0)
 nn_istate=0

 # DIMG file format: 1 file for all processors (F) or by processor (T)
 ln_dimgnnn=.false.

 # mask land points in NetCDF outputs (costly: + ~15%)
 ln_mskland=.false.

 # clobber (overwrite) an existing file
 ln_clobber=.false.

 # chunksize (bytes) for NetCDF file (works only with iom_nf90 routines)
 nn_chunksz=0
    
###### Massively Parallel Distribution for AGRIF zoom ("key_agrif" && "key_mpp_dyndist")

   # jpni  number of processors following i
   jpni=18

   # jpnj  number of processors following j
   jpnj=16

   # jpnij number of local domains
   jpnij=288

# print settings
   # &namctl        #  Control prints & Benchmark
   # Set ln_ctl = T and nn_print = 1 to get debugging output
   # Set ln_ctl = F and nn_print = 0 to get normal output
   ln_ctl=.false.   #  trends control print (expensive!)
   nn_print=0       #  level of print (0 no extra print)
#==================================================================================================

#-----------------------------------------------
#               SYSTEM SETTINGS
#
#         DO NOT EDIT BELOW THIS POINT 
# 
#----------------------------------------------

# You must know EXACTLY what you are doing and you assume FULL liability. 
 
 

# Seriously, just don't do it. 



# If you mess this up, things are going to melt down.



# Really? 


##################################################################

# Common functions
. ${SEQ_EXP_HOME}/bin/CanNEMO_shell_functions.sh

# Set parameters to 0 or 1.
ToF with_nemo_diag
ToF nemo_carbon
ToF nemo_trc
ToF nemo_cmoc
ToF pisces_offline
if [ $nn_ice -ge 2 ]; then
  with_ice=on
else
  with_ice=off
fi
ToF with_ice
ToF nemo_from_rest
ToF nemo_force_namelists
ToF nemo_save_hist
ToF nemo_carbon
ToF NEMO_CLEAN_TMP
ToF nemo_dump
ToF nemo_dump_rs
ToF nemo_del
ToF nemo_del_rs
ToF nemo_rtd
ToF nemo_rtd_from_rs
ToF nemoval_diag
ToF nemo_cfc

PATH=./:$PATH
export PATH

cn_exp=$runid

# Part of the input ocean restart name (default restart)
# The input ocean restart file name will be ${cn_ocerst_in}_MMMM.nc
# where MMMM is the MPI task number (I4.4)
cn_ocerst_in="restart"

# Part of output ocean restart name (default restart)
# The output ocean restart file name will be ${cn_exp}_NNNNNNNN_${cn_ocerst_out}_MMMM.nc
# where NNNNNNNN is the time step (I8.8) and MMMM is the MPI task number (I4.4)
cn_ocerst_out="restart"

# Part of input ice restart name (default restart_ice_in)
# The input ice restart file name will be ${cn_icerst_in}_MMMM.nc
# where MMMM is the MPI task number (I4.4)
cn_icerst_in="restart_ice"

# Part of output ice restart name (default restart_ice)
# The output ice restart file name will be ${cn_exp}_NNNNNNNN_${cn_icerst_out}_NNNN.nc
# where NNNNNNNN is the time step (I8.8) and MMMM is the MPI task number (I4.4)
cn_icerst_out="restart_ice"

# Part of input pisces restart name (default restart_trc_in)
# The input pisces restart file name will be ${cn_trcrst_in}_MMMM.nc
# where MMMM is the MPI task number (I4.4)
cn_trcrst_in="restart_trc"

# Part of output pisces restart name (default restart_trc)
# The output pisces restart file name will be ${cn_exp}_NNNNNNNN_${cn_trcrst_out}_NNNN.nc
# where NNNNNNNN is the time step (I8.8) and MMMM is the MPI task number (I4.4)
cn_trcrst_out="restart_trc"

#
#=====================================================================================

# These are temporary variables being set to point towards locations 
# for storage and execution, and will ultimately be set in the "environment". 
# Change to "hall1" for hare and "hall2" for brooks.
# hall=''
# TRUE_HOST=${TRUE_HOST}
# case ${TRUE_HOST} in
#   brooks*) hall=hall2 ;;
#   hare*) hall=hall1 ;;
#   eccc-ppp1*) hall=hall1 ;;
#   eccc-ppp2*) hall=hall2 ;;
#       *) hall=hall1 ;;
# esac

SITESTORE_ROOT="/space/${hall}/sitestore/eccc/crd/ccrn/"
SITESTORE_PATH="$SITESTORE_ROOT/users/$USER"
# This is where the output will end up.
OUTPUT_PATH="$SITESTORE_ROOT/users/$USER/$RUNID/"

config_dir=${SEQ_EXP_HOME}/CanNEMO_tmp_src/nemo/CONFIG/$config/
storage_dir=/home/ords/crd/ccrn/$USER/$RUNID
