#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
# NEMO postlude
# Revised NEMO history file pattens to be consistent with those in AGCM
#                                                    - Jan/2018, D.Yang
#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
#
# Summary of tasks:
#   - save history files
#   - create restart archive
#       - includes the main netcdf files and many other extra files added for 
#         logging purposes
#
  # ======================================================
  # Make sure we have necessary variables from parent code
  # ======================================================
  is_defined $nemo_hist_file_suffix_list_save   || bail "nemo_postlude: nemo_hist_file_suffix_list_save must be defined!"
  is_defined $nemo_hist_file_freq_list_save     || bail "nemo_postlude: nemo_hist_file_freq_list_save must be defined!"
  is_defined $runid                             || bail "nemo_postlude: runid must be defined!"
  is_defined $chunk_start_year                  || bail "nemo_postlude: chunk_start_year must be defined!"
  is_defined $chunk_start_month                 || bail "nemo_postlude: chunk_start_month must be defined!"
  is_defined $chunk_stop_year                   || bail "nemo_postlude: chunk_stop_year must be defined!"
  is_defined $chunk_stop_month                  || bail "nemo_postlude: chunk_stop_month must be defined!"
  is_defined $chunk_stop_day                    || bail "nemo_postlude: chunk_stop_day must be definied!"
  is_defined $uxxx                              || bail "nemo_postlude: uxxx must be defined!"

  #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
  #=#=#=#     Post processing     #=#=#=#
  #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#

  echo " "
  echo "#################### NEMO post processing #################"

  # The job name
  cn_exp=$runid

  # Part of the input ocean restart name (default restart)
  # The input ocean restart file name will be ${cn_ocerst_in}_MMMM.nc
  # where MMMM is the MPI task number (I4.4)
  cn_ocerst_in="restart"

  # Part of output ocean restart name (default restart)
  # The output ocean restart file name will be ${cn_exp}_NNNNNNNN_${cn_ocerst_out}_MMMM.nc
  # where NNNNNNNN is the time step (I8.8) and MMMM is the MPI task number (I4.4)
  cn_ocerst_out="restart"

  # Part of input ice restart name (default restart_ice_in)
  # The input ice restart file name will be ${cn_icerst_in}_MMMM.nc
  # where MMMM is the MPI task number (I4.4)
  cn_icerst_in="restart_ice_in"

  # Part of output ice restart name (default restart_ice)
  # The output ice restart file name will be ${cn_exp}_NNNNNNNN_${cn_icerst_out}_NNNN.nc
  # where NNNNNNNN is the time step (I8.8) and MMMM is the MPI task number (I4.4)
  cn_icerst_out="restart_ice"

  # Part of input pisces restart name (default restart_trc_in)
  # The input pisces restart file name will be ${cn_trcrst_in}_MMMM.nc
  # where MMMM is the MPI task number (I4.4)
  cn_trcrst_in="restart_trc_in"

  # Part of output pisces restart name (default restart_trc)
  # The output pisces restart file name will be ${cn_exp}_NNNNNNNN_${cn_trcrst_out}_NNNN.nc
  # where NNNNNNNN is the time step (I8.8) and MMMM is the MPI task number (I4.4)
  cn_trcrst_out="restart_trc"

  # After the job has run remove input restart files (if any)
  [ -s input_ocn_restart_file_names ] && cat input_ocn_restart_file_names | xargs rm -f
  [ -s input_ice_restart_file_names ] && cat input_ice_restart_file_names | xargs rm -f
  [ -s input_trc_restart_file_names ] && cat input_trc_restart_file_names | xargs rm -f

  # Should we save the NEMO restart to DATAPATH

    # The file saved_history_file_names will contain a list of saved files to be archived.
    # Since "save" will blindly transpose all file name characters to lower case before
    # saving to DATAPATH, we must keep track of the case of these files in some way.
    # The file saved_history_FILE_NAMES will contain the same list of file names that
    # saved_history_file_names contains but it will preserve case. (Large pita)
    rm -f saved_history_file_names
    touch saved_history_file_names
    rm -f saved_history_FILE_NAMES
    touch saved_history_FILE_NAMES

    # Translate history file suffix/freq lists into arrays and compare lengths
    nemo_hist_file_suffix_list_array_save=($nemo_hist_file_suffix_list_save)
    nemo_hist_file_freq_list_array_save=($nemo_hist_file_freq_list_save)
    n_suffix=${#nemo_hist_file_suffix_list_array_save[@]}
    n_freq=${#nemo_hist_file_freq_list_array_save[@]}
    if [ "$n_suffix" -ne "$n_freq" ]; then
      bail "ERROR: nemo_hist_file_suffix_list_array_save and nemo_hist_file_freq_list_array_save MUST have the same number of elements."
    fi

    # ====================
    # Save History Files
    # ====================
    nemo_save_hist=${nemo_save_hist:-on}
    ToF nemo_save_hist

    nemo_start_date=$(printf "%04d%02d%02d" $chunk_start_year $chunk_start_month 1)
    nemo_stop_date=$(printf "%04d%02d%02d" $chunk_stop_year $chunk_stop_month $chunk_stop_day)
    if [ $nemo_save_hist -eq 1 ] ; then
      for i in $(seq 0 $(($n_suffix-1))); do
          sfx=${nemo_hist_file_suffix_list_array_save[$i]}
          freq=${nemo_hist_file_freq_list_array_save[$i]}
          pfx=${cn_exp}_${freq}_${nemo_start_date}_${nemo_stop_date}_$sfx
          echo $pfx
          fnpatt=${pfx}_0000.nc
          if [ -s "$fnpatt" ]; then
             echo "found $fnpatt"
             rm -rf $pfx
             mkdir $pfx
             mv ${pfx}_[0-9]*.nc $pfx
             date_string=$(printf "%04d_m%02d" $chunk_start_year $chunk_start_month)
             ncsave=${cn_exp}_${date_string}_${freq}_${sfx}
             save $pfx ${uxxx}_$ncsave && echo "${uxxx}_$ncsave" >> saved_history_FILE_NAMES
          elif [ -s "${pfx}.nc" ]; then
             echo "found ${pfx}.nc"
             date_string=$(printf "%04d_m%02d" $chunk_start_year $chunk_start_month)
             ncsave=${cn_exp}_${date_string}_${freq}_${sfx}
             save ${pfx}.nc ${uxxx}_${ncsave}.nc && echo "${uxxx}_${ncsave}.nc" >> saved_history_FILE_NAMES
          fi
       done

      # Add the mesh_mask file created by the model, if present
      if [ -s mesh_mask_0000.nc ]; then
         rm -f rs_mesh_mask.nc
         pfx=mesh_mask
         rm -rf $pfx
         mkdir $pfx
         mv $pfx*.nc $pfx
         date_string=$(printf "%04d_m%02d" $chunk_start_year $chunk_start_month)
         ncsave=${cn_exp}_${date_string}_mesh_mask
         save $pfx ${uxxx}_$ncsave && echo "${uxxx}_$ncsave" >> saved_history_FILE_NAMES
      fi
    fi

    if [ -s saved_history_FILE_NAMES ]; then
      # Once files have saved to DATAPATH create a file that is
      # equivalent to the file saved_history_FILE_NAMES except that all file names
      # contained therein are lower case.
      rm -f saved_history_file_names
      cat saved_history_FILE_NAMES | tr '[A-Z]' '[a-z]' > saved_history_file_names
    fi

    # ================================
    # Save the current restart files.
    # ================================
    nemo_save_rs=${nemo_save_rs:-on}
    ToF nemo_save_rs

    # MAIN RESTART FILES
    if [ $nemo_save_rs -eq 1 ]; then
        # Collect up the current restarts and save them to DATAPATH
        rm -rf NEXT_RESTART_ARC
        mkdir NEXT_RESTART_ARC

        # Identify the last NEMO timestep, which will be in the rs names
        end_step=$(get_namelist_var nn_itend namelist | awk '{printf "%8.8d",$1}')

        # Look for ocean restart files matching the pattern below
        eval ofnpatt=${cn_exp}_${end_step}_${cn_ocerst_out}_\[0-9\]\[0-9\]\[0-9\]\[0-9\].nc
        found_rs=`(ls -1 $ofnpatt || : ) 2>/dev/null`
        if [ -z "$found_rs" ]; then
          # No ocean restart files exist
          bail "Attempting to save restarts but no ocean restart files exist. Looking for $ofnpatt"
        else
          mv $found_rs NEXT_RESTART_ARC
        fi

        if [ $nemo_trc -eq 1 ] || [ $nemo_carbon -eq 1 ]; then
          # Look for pisces restart files matching the pattern below
          eval pfnpatt=${cn_exp}_${end_step}_${cn_trcrst_out}_\[0-9\]\[0-9\]\[0-9\]\[0-9\].nc
          found_rs=`(ls -1 $pfnpatt || : ) 2>/dev/null`
          if [ -z "$found_rs" ]; then
            # No pisces restart files exist
            bail "Attempting to save restarts but no pisces restart files exist. Looking for $pfnpatt"
          else
            mv $found_rs NEXT_RESTART_ARC
          fi
        fi

      if [ $with_ice -eq 1 ]; then
         # Look for ice restart files
         eval ifnpatt=${cn_exp}_${end_step}_${cn_icerst_out}_\[0-9\]\[0-9\]\[0-9\]\[0-9\].nc
         found_rs=`(ls -1 $ifnpatt || : ) 2>/dev/null`
         if [ -z "$found_rs" ]; then
            # No ice restart files exist
            bail "Attempting to save restarts but no ice restart files exist. Looking for $ifnpatt"
         else
            mv $found_rs NEXT_RESTART_ARC
         fi
      fi

      # ADD ADDITIONAL FILES
      tarlist=''

      # namelist files
      cp -f namelist rs_namelist || :
      [ -s rs_namelist ] && tarlist="$tarlist rs_namelist"
      cp -f namelist_top rs_namelist_top || :
      [ -s rs_namelist_top ] && tarlist="$tarlist rs_namelist_top"
      cp -f namelist_ice rs_namelist_ice || :
      [ -s rs_namelist_ice ] && tarlist="$tarlist rs_namelist_ice"
      if [ $nemo_carbon -eq 1 ]; then
        cp -f namelist_pisces rs_namelist_pisces || :
        [ -s rs_namelist_pisces ]  && tarlist="$tarlist rs_namelist_pisces"

        if [ $nemo_cmoc -eq 1 ]; then
          cp -f namelist_cmoc rs_namelist_cmoc || :
          if [ -s rs_namelist_cmoc ]; then
            chmod u+rw rs_namelist_cmoc
            tarlist="$tarlist rs_namelist_cmoc"
          fi
        fi
      fi

      # current executable
      rm -f rs_${nemo_exec}
      cp -f ${nemo_exec} rs_${nemo_exec} || :
      [ -s rs_${nemo_exec} ] && tarlist="$tarlist rs_${nemo_exec}"

      if [[ "$runmode" == NEMO-AGCM-dcpp-assim* ]] ; then
        # Add a EMPave data for nn_fwb=2
        cp -f EMPave.dat rs_EMPave.dat || :
        [ -s rs_EMPave.dat ] && tarlist="$tarlist rs_EMPave.dat"
      fi

      # time step file
      cp -f time.step rs_time.step || :
      [ -s rs_time.step ] && tarlist="$tarlist rs_time.step"

      # ocean output file
      cp -f ocean.output rs_ocean.output || :
      [ -s rs_ocean.output ] && tarlist="$tarlist rs_ocean.output"

      # checksum file of ocean prognostic state (for bit identity checks)
      cp -f final.state rs_final.state || :
      [ -s rs_final.state ] && tarlist="$tarlist rs_final.state"

      # ocean timing stats
      cp -f timing.output rs_timing.output || :
      [ -s rs_timing.output ] && tarlist="$tarlist rs_timing.output"

      # list of history files that are saved (Needed?)
      cp -f saved_history_file_names rs_saved_history_file_names || :
      [ -s rs_saved_history_file_names ] && tarlist="$tarlist rs_saved_history_file_names"

      # Access log for nemo? (Needed?)
      cp -f xnemo_access_log rs_xnemo_access_log || :
      [ -s rs_xnemo_access_log ] && tarlist="$tarlist rs_xnemo_access_log"

      # the mesh mask file
      [ -s rs_mesh_mask.nc ] && tarlist="$tarlist rs_mesh_mask.nc"

      # MOVE ADDITIONAL FILES INTO ARCHIVE
      mv -f $tarlist NEXT_RESTART_ARC || :

      # SAVE
      if [ -n "$model_rs" ]; then
        rs_out=${model_rs}nemors
      else
        rs_out=${uxxx}_${cn_exp}_${nemo_stop_date}_restart.tar
      fi
      save NEXT_RESTART_ARC $rs_out
      release NEXT_RESTART_ARC
    fi

    # Echo ocean output to stdout, if requested
    nemo_write_stdout=on
    ToF nemo_write_stdout
    if [ $nemo_write_stdout -eq 1 ]; then
      if [ -s ocean.output ]; then
        echo " "
        echo "########################## ocean output ##########################"
        echo " "
        cat ocean.output
      fi
    fi
