#!/usr/bin/python
"""
nval_plt_types

Parse out the unique filetypes from a plot list yaml file.

Clint Seinen, January 18, 2018
"""
import yaml
import argparse

def get_filetypes(filename):
    
    # load list of different plots; each plot is a dictionary object.
    plots = []
    for data in yaml.load_all(open(filename)):
        plots.append(data)

    # get list of filetypes
    filetypes = []
    for plot in plots:
        filetypes.append(plot['ifile'])

    # get unique filetypes
    unique_filetypes = list(set(filetypes))
    
    return unique_filetypes

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    
    #====================#
    # Required Arguments #
    #====================#
    parser.add_argument("filename", action = "store", help = "the name of the plot list yaml file that must be processed. This file must match the yaml format for plot lists used by the NemoView webapp, see yaml_files/example_cmoc.yaml for an example")

    #========================#
    # Parse the CL arguments #
    #========================#
    args = parser.parse_args()
    filename = args.filename

    #======================#
    # Get unique filetypes #
    #======================#
    unique_filetypes = get_filetypes(filename)

    #===========================================#
    # Print unique filetypes to standard output #
    #===========================================#
    unique_filetypes = " ".join(unique_filetypes)
    print(unique_filetypes)
