# If nemo_rtd is true then execute the nemo run time diagnostics
# Enable cmip6_diag option (D. YANG, OCT 2017):
# if with_nemo_diag is true, then compute CMIP6 offline diagnostics.

if [ $nemo_rtd -eq 1 ]; then
  # Get the input files for this chunk
  ln -s $OUTPUT_PATH/${cn_exp}_1m_${NEMO_CHUNK_START_DATE}_${NEMO_CHUNK_END_DATE}_*.nc .

  # Specify the location of some NCO tools
  # These are required for manipulation of the netcdf files
  #NCOBIN=/users/tor/acrn/rls/local/aix64/bin
  #NCRCAT="$NCOBIN/ncrcat"
  #NCATTED="$NCOBIN/ncatted"
  acc_cp orca_mesh_mask $orca_grid_info 

  # The RTD will be continued from the RTD files in the OUTPUT_PATH dir.
  OLD_RTD_LOC=$OUTPUT_PATH

  # Invoke the nemo run time diagnostics
  #--------------
  # Physics RTD
  #--------------
  if [ $pisces_offline -eq 0 ]; then
      # Get the old RTD, if it exists.
      [ -s $OLD_RTD_LOC/sc_${cn_exp}_nemo_physical_rtd.nc ]  && cp $OLD_RTD_LOC/sc_${cn_exp}_nemo_physical_rtd.nc ./nemo_physical_rtd.nc 
      # Rename the input files for RTD.
      ln -s  ${cn_exp}_1m_${NEMO_CHUNK_START_DATE}_${NEMO_CHUNK_END_DATE}_grid_T.nc grid_t
      ln -s ${cn_exp}_1m_${NEMO_CHUNK_START_DATE}_${NEMO_CHUNK_END_DATE}_grid_U.nc  grid_u
      ln -s ${cn_exp}_1m_${NEMO_CHUNK_START_DATE}_${NEMO_CHUNK_END_DATE}_grid_V.nc grid_v
      ln -s ${cn_exp}_1m_${NEMO_CHUNK_START_DATE}_${NEMO_CHUNK_END_DATE}_grid_W.nc grid_w

      # Access the rtd executable
      [ -z "$physical_rtd_exe" ] && bail "physical_rtd_exe is not defined."
      physical_rtd_exe_path=${storage_dir}/executables/${physical_rtd_exe}
      if [[ -e "$physical_rtd_exe_path" ]]; then 
          cp $physical_rtd_exe_path .
      else
          bail "$physical_rtd_exe does not exist in $(dirname $physical_rtd_exe_path)! Has it been compiled?"
      fi

      # If nemo_physical_rtd.nc exists, append to it. Else start it, and add version attribute.
      if [ -f "nemo_physical_rtd.nc" ]; then 
          # Create run time diagnostics for physical ocean variables
          ./$physical_rtd_exe ${NEMO_CHUNK_START_YEAR} ${NEMO_CHUNK_START_MONTH}
            #echo $curr_rtd_file >> $rtd_file_del_list
      else
          # Create run time diagnostics for physical ocean variables
          ./$physical_rtd_exe ${NEMO_CHUNK_START_YEAR} ${NEMO_CHUNK_START_MONTH}
          #${NCATTED} -O -a RTD_version,global,a,c,"${physical_rtd_exe}" nemo_physical_rtd.nc
      fi

      # Save the rtd file created by physical_rtd_exe
      #new_rtd_file=sc_${runid}_${stop_date}_nemo_physical_rtd.nc
      #save nemo_physical_rtd.nc $new_rtd_file
      #echo $new_rtd_file >> $rtd_file_dump_list

      #--------------
      # Sea-ice RTD
      #--------------
      if [ $nn_ice -ne 0 ]; then
        # Get the old RTD, if it exists.
        [ -s $OLD_RTD_LOC/sc_${cn_exp}_nemo_ice_rtd.nc ]       && cp $OLD_RTD_LOC/sc_${cn_exp}_nemo_ice_rtd.nc ./nemo_ice_rtd.nc 
        # Rename the files for RTD.
        ln -s ${cn_exp}_1m_${NEMO_CHUNK_START_DATE}_${NEMO_CHUNK_END_DATE}_icemod.nc icemod

        # Access the rtd executable
        [ -z "$ice_rtd_exe" ] && bail "ice_rtd_exe is not defined."
        ice_rtd_exe_path=${storage_dir}/executables/${ice_rtd_exe}
        if [[ -e "$ice_rtd_exe_path" ]]; then 
            cp $ice_rtd_exe_path .
        else
            bail "$ice_rtd_exe does not exist in $(dirname $ice_rtd_exe_path)! Has it been compiled?"
        fi

        # If nemo_ice_rtd.nc exists, append to it. Else start it, and add version attribute.
        if [ -f "nemo_ice_rtd.nc" ]; then 
            # Create run time diagnostics for carbon variables
            ./$ice_rtd_exe ${NEMO_CHUNK_START_YEAR} ${NEMO_CHUNK_START_MONTH}
              #echo $curr_rtd_file >> $rtd_file_del_list
        else
            # Create run time diagnostics for carbon variables
            ./$ice_rtd_exe ${NEMO_CHUNK_START_YEAR} ${NEMO_CHUNK_START_MONTH}
            #${NCATTED} -O -a RTD_version,global,a,c,"${ice_rtd_exe}" nemo_ice_rtd.nc
        fi

        # Save the rtd file created by carbon_rtd_exe
        #new_rtd_file=sc_${runid}_${stop_date}_nemo_ice_rtd.nc
        #save nemo_ice_rtd.nc $new_rtd_file
        #echo $new_rtd_file >> $rtd_file_dump_list

      fi
  fi # if [pisces_offline -eq 0 ]

  #--------------
  # Carbon RTD
  #--------------
  if [ $nemo_carbon -eq 1 ]; then

    # Get the old RTD, if it exists.
    [ -s $OLD_RTD_LOC/sc_${cn_exp}_nemo_carbon_rtd.nc ]  && cp $OLD_RTD_LOC/sc_${cn_exp}_nemo_carbon_rtd.nc ./nemo_carbon_rtd.nc 

    # Rename files for RTD
    ln -s  ${cn_exp}_1m_${NEMO_CHUNK_START_DATE}_${NEMO_CHUNK_END_DATE}_ptrc_T.nc ptrc_t
    ln -s  ${cn_exp}_1m_${NEMO_CHUNK_START_DATE}_${NEMO_CHUNK_END_DATE}_diad_T.nc diad_t
    [ -e ptrc_t ] || bail "Failed in carbon RTD: ptrc_t missing"
    [ -e diad_t ] || bail "Failed in carbon RTD: diad_t missing"

    # Access the rtd executable
    [ -z "$carbon_rtd_exe" ] && bail "carbon_rtd_exe is not defined."
    carbon_rtd_exe_path=${storage_dir}/executables/${carbon_rtd_exe}
    if [[ -e "${carbon_rtd_exe_path}" ]]; then 
        cp $carbon_rtd_exe_path .
    else
        bail "$carbon_rtd_exe does not exist in $(dirname $carbon_rtd_exe_path)! Has it been compiled?"
    fi

    # If nemo_ice_rtd.nc exists, append to it. Else start it, and add version attribute.
    if [ -z "nemo_carbon_rtd.nc" ]; then 
        ./$carbon_rtd_exe ${NEMO_CHUNK_START_YEAR} ${NEMO_CHUNK_START_MONTH}
          #echo $curr_rtd_file >> $rtd_file_del_list
    else
        # Create run time diagnostics for carbon variables
        ./$carbon_rtd_exe ${NEMO_CHUNK_START_YEAR} ${NEMO_CHUNK_START_MONTH}
        #${NCATTED} -O -a RTD_version,global,a,c,"${carbon_rtd_exe}" nemo_carbon_rtd.nc
    fi

    # Save the rtd file created by carbon_rtd_exe
    #new_rtd_file=sc_${runid}_${stop_date}_nemo_carbon_rtd.nc
    #save nemo_carbon_rtd.nc $new_rtd_file
    #echo $new_rtd_file >> $rtd_file_dump_list

  fi

  # Clean up
  release grid_t
  release grid_u
  release grid_v
  release grid_w
  release icemod
  release orca_mesh_mask
  if [ $nemo_carbon -eq 1 ]; then
    # PISCES related files
    release ptrc_t
    release diad_t
  fi

  # copy out RTD to sitestore (OUTPUTH_PATH), add it to the tar archive, and copy to the location for sync with Victoria
  for i in *rtd.nc; do
     mv $i sc_${cn_exp}_$i
     cp sc_${cn_exp}_$i $OUTPUT_PATH/sc_${cn_exp}_$i
     cp sc_${cn_exp}_$i ${SITESTORE_ROOT}/prod/rtd/nemo_rtd/sc_${cn_exp}_$i
     tar -rvf $OUTPUT_PATH/${cn_exp}_${NEMO_CHUNK_END_DATE}_restart.tar sc_${cn_exp}_$i
  done

fi # if [ $nemo_rtd -eq 1 ]

#-----------------------------------------
# Produce NemoVal plots if nemoval_diag=on 
#-----------------------------------------
if [ $nemoval_diag -eq 1 ] ; then    

    # determine what action should be taken
    nemoval_mod=$((NEMO_CHUNK_END_YEAR % nemoval_repeat_period)) 

    ## how many years per repeat period where no plotting/merging is needed
    noplt_period=$((nemoval_repeat_period - nemoval_avg_period)) 

    #    is this a plot yr?   OR  is it a merging period?
    if [ $nemoval_mod -eq 0 ] || [ $nemoval_mod -gt $noplt_period ]; then
        # plotting or file merging should happen, set CDO var to skip duplicate times after
        #   first occurence
        export SKIP_SAME_TIME=1
        
        # copy the NView settings files and source
        cp $nemoval_root/data/NViewConfig.sh .
        . ./NViewConfig.sh
        
        # determine plot years
        if [ $nemoval_mod -eq 0 ]; then
            pltey=$NEMO_CHUNK_END_YEAR
        else
            nval_tmp=$((NEMO_CHUNK_END_YEAR + (nemoval_repeat_period - nemoval_mod)))
            pltey=$(printf "%04d" $nval_tmp)
        fi
        # note: the + 1 is required since we want plots to INCLUDE the final year
        pltsy=$(printf "%04d" $((pltey - nemoval_avg_period + 1))) 

        # only try to produce plots if there is actually data associated with this run
        # for these dates. This is to avoid problems when a run is started at a date
        # during a merging period or on a plot year.
        nrsy=`getdef resources NEMO_RUN_START_YEAR`
        if [ $pltsy -ge $nrsy ]; then
        
            # get previous chunk end year
            prcey=$(printf "%04d" $((NEMO_CHUNK_START_YEAR - 1)))

            # set tmp merged file suffix
            new_int_mrg_file_suf="${OUTPUT_PATH}model_rtime_nval_tmp_${RUNID}_${pltsy}_${NEMO_CHUNK_END_YEAR}_"
            prev_int_mrg_file_suf="${OUTPUT_PATH}model_rtime_nval_tmp_${RUNID}_${pltsy}_${prcey}_"

            # determine plot list and the associated filetypes
            if [ -z $nemoval_pltyml ]; then 
                # use a default plot list
                if [ $nemo_carbon -eq 1 ] && [ $nemo_cmoc -eq 1 ] && [ $pisces_offline -eq 0 ]; then
                    nemoval_pltyml='cmoc+physics+ice.yaml'
                    [ $nn_ice -eq 0 ] && nemoval_pltyml='cmoc+physics.yaml'
                elif [ $nemo_carbon -eq 1 ] && [ $nemo_cmoc -eq 1 ] && [ $pisces_offline -eq 1 ]; then
                    nemoval_pltyml='cmoc.yaml'
                elif [ $nemo_carbon -eq 1 ] && [ $nemo_cmoc -eq 0 ] && [ $pisces_offline -eq 0 ]; then
                    nemoval_pltyml='canoe+physics+ice.yaml'
                    [ $nn_ice -eq 0 ] && nemoval_pltyml='canoe+physics.yaml'
                elif [ $nemo_carbon -eq 1 ] && [ $nemo_cmoc -eq 0 ] && [ $pisces_offline -eq 1 ]; then
                    nemoval_pltyml='canoe.yaml'
                elif [ $nemo_carbon -eq 0 ]; then
                    nemoval_pltyml='physics+ice.yaml'
                    [ $nn_ice -eq 0 ] && nemoval_pltyml='physics.yaml'
                else
                    # run can't use default plots 
                    bail "Unable to produce NemoVal plots from experiment.cfg settings"
                fi
                nemoval_pltyml_pth="${nemoval_root}/data/model_rtime_files/def_yamls"
            fi
            
            # determine plot types associated with yaml
            filetypes=$($SEQ_EXP_HOME/bin/nval_plt_types $nemoval_pltyml_pth/$nemoval_pltyml)

            if [ $NEMO_CHUNK_END_YEAR -eq $pltey ]; then
                # copy the template over from nemoval_root
                cp $nemoval_root/data/model_rtime_files/model_rtime_template .

                #-----------------------------------------------------------------
                # Perform final .nc handling, time avg, and store in nemoval_root
                #-----------------------------------------------------------------
                for fl in $filetypes; do
                    fl=$(echo $fl | sed 's/_t/_T/;s/_u/_U/;s/_w/_W/;s/_v/_V/') 
                    
                    if [ $NEMO_CHUNK_START_YEAR -le $pltsy ]; then
                        # then no merged file has been created yet (the plot period is contained in one nemo chunk)
                        # Select out the desired dates instead of merging.
                        inpt_fl="$OUTPUT_PATH/${RUNID}_1m_${NEMO_CHUNK_START_DATE}_${NEMO_CHUNK_END_DATE}_${fl}.nc"
                        outpt_fl="${OUTPUT_PATH}model_rtime_nval_tmp_${RUNID}_${pltsy}_${pltey}_${fl}.nc"
                        strt="$pltsy-01-01T00:00:00"
                        end="$pltey-12-31T23:59:59"

                        $CDO_PTH/cdo seldate,$strt,$end $inpt_fl $outpt_fl
                    else
                        # perform final merger
                        inpt_fl1="${prev_int_mrg_file_suf}${fl}.nc" 
                        inpt_fl2="$OUTPUT_PATH/${RUNID}_1m_${NEMO_CHUNK_START_DATE}_${NEMO_CHUNK_END_DATE}_${fl}.nc"
                        outpt_fl="${new_int_mrg_file_suf}${fl}.nc"

                        $CDO_PTH/cdo mergetime $inpt_fl1 $inpt_fl2 $outpt_fl
                        rm $inpt_fl1
                    fi

                    inpt_fl="$outpt_fl"
                    outpt_fl="$MRG_DATA_DIR/mc_${RUNID}_1m_${pltsy}0101_${pltey}1231_${fl}.nc"
                    $CDO_PTH/cdo timmean $inpt_fl $outpt_fl

                    rm $inpt_fl 
                done

                #--------------------
                # launch plotting job
                #--------------------
                
                # replace name of python environment with its absolute path
                PY_ENV="/home/$SCI_ID/.conda/envs/$PY_ENV"
                sed -i "s#PY_ENV=.*#PY_ENV=\"$PY_ENV\"#g" NViewConfig.sh

                # replace activate exectubale with its full/absolute path
                ACTV_EXE="$PY_ENV/bin/activate"
                sed -i "s#ACTV_EXE=.*#ACTV_EXE=\"$ACTV_EXE\"#g" NViewConfig.sh
                
                # replace SCI_ID with user running maestro
                SCI_ID=$(whoami)
                sed -i "s#SCI_ID=.*#SCI_ID=\"$SCI_ID\"#g" NViewConfig.sh
                
                # set plotting machine to match the hall set by the experiment
                if [ $hall == "hall1" ]; then
                    RUN_MACH="ppp1"
                elif [ $hall == "hall2" ]; then
                    RUN_MACH="ppp2"
                else
                    bail "hall must be set to hall1 or hall2..see experiment.cfg"
                fi
                
                # set mask to be used by the plotting scripts
                if [ $nemoval_res == "2d" ]; then
                    MASK="orca2_tmask_miss.nc"
                elif [ $nemoval_res == "1d" ]; then
                    MASK="orca1_tmask_miss.nc"
                elif [ $nemoval_res == "025d" ]; then
                    MASK="orca025_tmask_miss.nc"
                else
                    bail "you selected a resolution not supported by NemoVal plots..either decrease resolution or turn off NemoVal plots"
                fi
                
                # get unique PID/DATE string to ID the plotting job 
                plt_ID=$$_`date +%Y-%m-%d%H%M%S`

                # produce job script
                cat model_rtime_template | sed "s#RUNID#$RUNID#g;s#START#$pltsy#g;\
                s#END#$pltey#g;s#SCI_ID#$SCI_ID#g;s#NV_USER#model_rtime_NemoVal#g;s#NVAL_ROOT#$NVAL_ROOT#g;\
                s#PID#$plt_ID#g;s#RUN_MACH#$RUN_MACH#g;s#VERSION#$DEF_NVAL_VER#g;\
                s#OBS_DIR#$NVAL_ROOT/data/$DEF_OBS_DIR#g;s#MASKFILE#$MASK#g;s#PLOTS#$nemoval_pltyml#g;\
                s#PLTYML_PATH#$nemoval_pltyml_pth#g;s#OBSERVED#$DEF_OBS_LST#g;s#PY_ENV#$PY_ENV#g;\
                s#ACTV_EXE#$ACTV_EXE#g;s#FILETYPES#$filetypes#g"\
                >> maestro_nvaljob_${RUNID}_${pltsy}_${pltey}

                # remove useless template and place NViewConfig.sh in NemoVal's staging directory
                if [ $? -eq 0 ]; then
                    mv NViewConfig.sh $nemoval_root/stg/NViewConfig_${plt_ID}_.sh
                    rm model_rtime_template
                fi
                
                # submit job
                rsub $RUN_MACH maestro_nvaljob_${RUNID}_${pltsy}_${pltey} > /dev/null 2>&1
                if [ $? -eq 0 ]; then
                    echo "NemoVal plotting job submitted"
                fi
            elif [ $NEMO_CHUNK_START_YEAR -le $pltsy ]; then
                #----------------------
                # start new merge file
                #----------------------

                # use seldate to cut off any extra time on the front end data record
                #   - implemented in case the nemoval average time doesn't line up with nemo_freq
                for fl in $filetypes; do
                    fl=$(echo $fl | sed 's/_t/_T/;s/_u/_U/;s/_w/_W/;s/_v/_V/') 
                    inpt_fl="${OUTPUT_PATH}${RUNID}_1m_${NEMO_CHUNK_START_DATE}_${NEMO_CHUNK_END_DATE}_${fl}.nc"
                    outpt_fl="${new_int_mrg_file_suf}${fl}.nc"
                    strt="$pltsy-01-01T00:00:00"
                    end="$NEMO_CHUNK_END_YEAR-12-31T23:59:59"

                    $CDO_PTH/cdo seldate,$strt,$end $inpt_fl $outpt_fl
                done
            else
                #--------------------------------
                # append to existing merged file
                #--------------------------------
                
                for fl in $filetypes; do
                    fl=$(echo $fl | sed 's/_t/_T/;s/_u/_U/;s/_w/_W/;s/_v/_V/') 
                    inpt_fl1="${prev_int_mrg_file_suf}${fl}.nc" 
                    inpt_fl2="${OUTPUT_PATH}${RUNID}_1m_${NEMO_CHUNK_START_DATE}_${NEMO_CHUNK_END_DATE}_${fl}.nc"
                    outpt_fl="${new_int_mrg_file_suf}${fl}.nc"

                    $CDO_PTH/cdo mergetime $inpt_fl1 $inpt_fl2 $outpt_fl
                    rm $inpt_fl1
                done
            fi
        fi
    fi
fi
